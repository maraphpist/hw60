<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_0a150352ead77b8f4825c5cb64eb12a87033c68131590abccc15d5143742aa77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_16e6c0c5394f6b6fef948c6860955b06a2327ae96dc828f84bd982b6fbecfed4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_16e6c0c5394f6b6fef948c6860955b06a2327ae96dc828f84bd982b6fbecfed4->enter($__internal_16e6c0c5394f6b6fef948c6860955b06a2327ae96dc828f84bd982b6fbecfed4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_827c5dff8db3092f81eb639114a0c4ade9dc7ea44b27388fd873f0b369115fa1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_827c5dff8db3092f81eb639114a0c4ade9dc7ea44b27388fd873f0b369115fa1->enter($__internal_827c5dff8db3092f81eb639114a0c4ade9dc7ea44b27388fd873f0b369115fa1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_16e6c0c5394f6b6fef948c6860955b06a2327ae96dc828f84bd982b6fbecfed4->leave($__internal_16e6c0c5394f6b6fef948c6860955b06a2327ae96dc828f84bd982b6fbecfed4_prof);

        
        $__internal_827c5dff8db3092f81eb639114a0c4ade9dc7ea44b27388fd873f0b369115fa1->leave($__internal_827c5dff8db3092f81eb639114a0c4ade9dc7ea44b27388fd873f0b369115fa1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
