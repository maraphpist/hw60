<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_1d248dcab04f8cbde7f9f83d30aa6fe4d619c84bcc9614f7da894e29a15f9d8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_616249e813c0190b7f402fc29406de63cdabbae5b8adb7f5fdd6c4d9af3a5090 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_616249e813c0190b7f402fc29406de63cdabbae5b8adb7f5fdd6c4d9af3a5090->enter($__internal_616249e813c0190b7f402fc29406de63cdabbae5b8adb7f5fdd6c4d9af3a5090_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_e9e9b2d017d8a9b2e192791ba24a242b09c4e7a62e2f352c02eaa017965bee40 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e9e9b2d017d8a9b2e192791ba24a242b09c4e7a62e2f352c02eaa017965bee40->enter($__internal_e9e9b2d017d8a9b2e192791ba24a242b09c4e7a62e2f352c02eaa017965bee40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_616249e813c0190b7f402fc29406de63cdabbae5b8adb7f5fdd6c4d9af3a5090->leave($__internal_616249e813c0190b7f402fc29406de63cdabbae5b8adb7f5fdd6c4d9af3a5090_prof);

        
        $__internal_e9e9b2d017d8a9b2e192791ba24a242b09c4e7a62e2f352c02eaa017965bee40->leave($__internal_e9e9b2d017d8a9b2e192791ba24a242b09c4e7a62e2f352c02eaa017965bee40_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_widget_compound.html.php");
    }
}
