<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_9c4f657469d396758145b0b399b9355e90943c3e96346630fc1eab2a16180b53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7c846b2f6a7ea6b0f0dca214d9e031acbb04c60c06ffeb8feb398eb31c51b021 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c846b2f6a7ea6b0f0dca214d9e031acbb04c60c06ffeb8feb398eb31c51b021->enter($__internal_7c846b2f6a7ea6b0f0dca214d9e031acbb04c60c06ffeb8feb398eb31c51b021_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        $__internal_353098247ffb2189746d261035cc6011f77ca73ed053386924ad758463cb0348 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_353098247ffb2189746d261035cc6011f77ca73ed053386924ad758463cb0348->enter($__internal_353098247ffb2189746d261035cc6011f77ca73ed053386924ad758463cb0348_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_7c846b2f6a7ea6b0f0dca214d9e031acbb04c60c06ffeb8feb398eb31c51b021->leave($__internal_7c846b2f6a7ea6b0f0dca214d9e031acbb04c60c06ffeb8feb398eb31c51b021_prof);

        
        $__internal_353098247ffb2189746d261035cc6011f77ca73ed053386924ad758463cb0348->leave($__internal_353098247ffb2189746d261035cc6011f77ca73ed053386924ad758463cb0348_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choice_widget_expanded.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget_expanded.html.php");
    }
}
