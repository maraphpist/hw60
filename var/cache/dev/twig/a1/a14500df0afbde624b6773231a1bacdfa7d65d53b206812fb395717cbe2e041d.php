<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_bc4ea1beb28828ce335227ee50b2e3d5971fbb5a697333240a240d7c558d8474 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ef77e7e8ca859aa151503c35464aa30b88f0991fb8f4bdb07dcac5eb6f61647 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ef77e7e8ca859aa151503c35464aa30b88f0991fb8f4bdb07dcac5eb6f61647->enter($__internal_8ef77e7e8ca859aa151503c35464aa30b88f0991fb8f4bdb07dcac5eb6f61647_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $__internal_6837f736c2889e7f846ec598da8737a64a9ae6ac158c7043e443fc22905bdbfc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6837f736c2889e7f846ec598da8737a64a9ae6ac158c7043e443fc22905bdbfc->enter($__internal_6837f736c2889e7f846ec598da8737a64a9ae6ac158c7043e443fc22905bdbfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8ef77e7e8ca859aa151503c35464aa30b88f0991fb8f4bdb07dcac5eb6f61647->leave($__internal_8ef77e7e8ca859aa151503c35464aa30b88f0991fb8f4bdb07dcac5eb6f61647_prof);

        
        $__internal_6837f736c2889e7f846ec598da8737a64a9ae6ac158c7043e443fc22905bdbfc->leave($__internal_6837f736c2889e7f846ec598da8737a64a9ae6ac158c7043e443fc22905bdbfc_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_de5fa7d41610fc7121324a8764b1e6eeaef613956207c364ae8a56d5089c351a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_de5fa7d41610fc7121324a8764b1e6eeaef613956207c364ae8a56d5089c351a->enter($__internal_de5fa7d41610fc7121324a8764b1e6eeaef613956207c364ae8a56d5089c351a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_060dc8fe5c02ac0bc5fde79701f3cc0a01cbfaa2d4d74b4183aede23a4638bd3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_060dc8fe5c02ac0bc5fde79701f3cc0a01cbfaa2d4d74b4183aede23a4638bd3->enter($__internal_060dc8fe5c02ac0bc5fde79701f3cc0a01cbfaa2d4d74b4183aede23a4638bd3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_060dc8fe5c02ac0bc5fde79701f3cc0a01cbfaa2d4d74b4183aede23a4638bd3->leave($__internal_060dc8fe5c02ac0bc5fde79701f3cc0a01cbfaa2d4d74b4183aede23a4638bd3_prof);

        
        $__internal_de5fa7d41610fc7121324a8764b1e6eeaef613956207c364ae8a56d5089c351a->leave($__internal_de5fa7d41610fc7121324a8764b1e6eeaef613956207c364ae8a56d5089c351a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:ChangePassword:change_password.html.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}
