<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_3940b12f38f4f22cd7582c0ade22f69829cb0a3d02a402a00fb82a273828a6d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a12a57f6744d3175b0166cb547c92248e8f8a09d273a627afab7fc31a257a69c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a12a57f6744d3175b0166cb547c92248e8f8a09d273a627afab7fc31a257a69c->enter($__internal_a12a57f6744d3175b0166cb547c92248e8f8a09d273a627afab7fc31a257a69c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $__internal_01a0991930f00fb83c79b75426c10deb5781f83f9a2a12a03122d303d4aa590a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_01a0991930f00fb83c79b75426c10deb5781f83f9a2a12a03122d303d4aa590a->enter($__internal_01a0991930f00fb83c79b75426c10deb5781f83f9a2a12a03122d303d4aa590a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a12a57f6744d3175b0166cb547c92248e8f8a09d273a627afab7fc31a257a69c->leave($__internal_a12a57f6744d3175b0166cb547c92248e8f8a09d273a627afab7fc31a257a69c_prof);

        
        $__internal_01a0991930f00fb83c79b75426c10deb5781f83f9a2a12a03122d303d4aa590a->leave($__internal_01a0991930f00fb83c79b75426c10deb5781f83f9a2a12a03122d303d4aa590a_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_16a73113eb0cdba64fca8db23cb2cfd05e325209516e33a9a59bc9b86d7ac3a9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_16a73113eb0cdba64fca8db23cb2cfd05e325209516e33a9a59bc9b86d7ac3a9->enter($__internal_16a73113eb0cdba64fca8db23cb2cfd05e325209516e33a9a59bc9b86d7ac3a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_796740456adaf23c2d5f884d5a09a36ce14b7cbc711222aea2e686ddd4879c60 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_796740456adaf23c2d5f884d5a09a36ce14b7cbc711222aea2e686ddd4879c60->enter($__internal_796740456adaf23c2d5f884d5a09a36ce14b7cbc711222aea2e686ddd4879c60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_796740456adaf23c2d5f884d5a09a36ce14b7cbc711222aea2e686ddd4879c60->leave($__internal_796740456adaf23c2d5f884d5a09a36ce14b7cbc711222aea2e686ddd4879c60_prof);

        
        $__internal_16a73113eb0cdba64fca8db23cb2cfd05e325209516e33a9a59bc9b86d7ac3a9->leave($__internal_16a73113eb0cdba64fca8db23cb2cfd05e325209516e33a9a59bc9b86d7ac3a9_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:request.html.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/request.html.twig");
    }
}
