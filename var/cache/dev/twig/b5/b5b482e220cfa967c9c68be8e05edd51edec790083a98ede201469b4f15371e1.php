<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_2d17f17f30d4f3ea6a640d73891facb6213a880f8e7e5d75994f5833324c3af7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_75f7ade867d6158d0a5f736c5ff350965de7c8c300ad57b2198da72a713154cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_75f7ade867d6158d0a5f736c5ff350965de7c8c300ad57b2198da72a713154cf->enter($__internal_75f7ade867d6158d0a5f736c5ff350965de7c8c300ad57b2198da72a713154cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $__internal_fade2623828da82870ff16bf3f6241a739ee5256881f3cf05c2041c702911ac7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fade2623828da82870ff16bf3f6241a739ee5256881f3cf05c2041c702911ac7->enter($__internal_fade2623828da82870ff16bf3f6241a739ee5256881f3cf05c2041c702911ac7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_75f7ade867d6158d0a5f736c5ff350965de7c8c300ad57b2198da72a713154cf->leave($__internal_75f7ade867d6158d0a5f736c5ff350965de7c8c300ad57b2198da72a713154cf_prof);

        
        $__internal_fade2623828da82870ff16bf3f6241a739ee5256881f3cf05c2041c702911ac7->leave($__internal_fade2623828da82870ff16bf3f6241a739ee5256881f3cf05c2041c702911ac7_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_8d164beadb85665c47c54e68e4302def081038b16c58c80e54597aa6c00364d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d164beadb85665c47c54e68e4302def081038b16c58c80e54597aa6c00364d3->enter($__internal_8d164beadb85665c47c54e68e4302def081038b16c58c80e54597aa6c00364d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_386259f231ce2db8ddfdb4cd4c194f2f3794a2ed4fd755909076ad1c91ea458b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_386259f231ce2db8ddfdb4cd4c194f2f3794a2ed4fd755909076ad1c91ea458b->enter($__internal_386259f231ce2db8ddfdb4cd4c194f2f3794a2ed4fd755909076ad1c91ea458b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_386259f231ce2db8ddfdb4cd4c194f2f3794a2ed4fd755909076ad1c91ea458b->leave($__internal_386259f231ce2db8ddfdb4cd4c194f2f3794a2ed4fd755909076ad1c91ea458b_prof);

        
        $__internal_8d164beadb85665c47c54e68e4302def081038b16c58c80e54597aa6c00364d3->leave($__internal_8d164beadb85665c47c54e68e4302def081038b16c58c80e54597aa6c00364d3_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:edit.html.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/edit.html.twig");
    }
}
