<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_04b7e365edc6d0270359b108e26ab84aed7e2c8714c81d85055c1b56c5fbfa66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_20a28cda6b4723f2fd1852b1492ba6fa05d553b2aeb69018b4bc295b6c2f7508 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_20a28cda6b4723f2fd1852b1492ba6fa05d553b2aeb69018b4bc295b6c2f7508->enter($__internal_20a28cda6b4723f2fd1852b1492ba6fa05d553b2aeb69018b4bc295b6c2f7508_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_95eac9eb62ad832ed9565301325300aeb3adff351da463bc2d296b60e9d289c8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_95eac9eb62ad832ed9565301325300aeb3adff351da463bc2d296b60e9d289c8->enter($__internal_95eac9eb62ad832ed9565301325300aeb3adff351da463bc2d296b60e9d289c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_20a28cda6b4723f2fd1852b1492ba6fa05d553b2aeb69018b4bc295b6c2f7508->leave($__internal_20a28cda6b4723f2fd1852b1492ba6fa05d553b2aeb69018b4bc295b6c2f7508_prof);

        
        $__internal_95eac9eb62ad832ed9565301325300aeb3adff351da463bc2d296b60e9d289c8->leave($__internal_95eac9eb62ad832ed9565301325300aeb3adff351da463bc2d296b60e9d289c8_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_7b59d4a3fe7e9561eb9ae27f8235689f1a9896dd7d130076adaf1953a98985b3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7b59d4a3fe7e9561eb9ae27f8235689f1a9896dd7d130076adaf1953a98985b3->enter($__internal_7b59d4a3fe7e9561eb9ae27f8235689f1a9896dd7d130076adaf1953a98985b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_78cc613b01fb2269612a92cdac182c85e9fd992345a2aa8a6e9ea98aea2957bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78cc613b01fb2269612a92cdac182c85e9fd992345a2aa8a6e9ea98aea2957bf->enter($__internal_78cc613b01fb2269612a92cdac182c85e9fd992345a2aa8a6e9ea98aea2957bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_78cc613b01fb2269612a92cdac182c85e9fd992345a2aa8a6e9ea98aea2957bf->leave($__internal_78cc613b01fb2269612a92cdac182c85e9fd992345a2aa8a6e9ea98aea2957bf_prof);

        
        $__internal_7b59d4a3fe7e9561eb9ae27f8235689f1a9896dd7d130076adaf1953a98985b3->leave($__internal_7b59d4a3fe7e9561eb9ae27f8235689f1a9896dd7d130076adaf1953a98985b3_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_1c4b486f8a82f008177da59982f32f8d4ba85469ac6769e0d04001110f7b8abb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c4b486f8a82f008177da59982f32f8d4ba85469ac6769e0d04001110f7b8abb->enter($__internal_1c4b486f8a82f008177da59982f32f8d4ba85469ac6769e0d04001110f7b8abb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_78db006d9b6f91696247955b9db7b83fe76a27b3aed622415bff6a416f5fb3b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78db006d9b6f91696247955b9db7b83fe76a27b3aed622415bff6a416f5fb3b4->enter($__internal_78db006d9b6f91696247955b9db7b83fe76a27b3aed622415bff6a416f5fb3b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_78db006d9b6f91696247955b9db7b83fe76a27b3aed622415bff6a416f5fb3b4->leave($__internal_78db006d9b6f91696247955b9db7b83fe76a27b3aed622415bff6a416f5fb3b4_prof);

        
        $__internal_1c4b486f8a82f008177da59982f32f8d4ba85469ac6769e0d04001110f7b8abb->leave($__internal_1c4b486f8a82f008177da59982f32f8d4ba85469ac6769e0d04001110f7b8abb_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
