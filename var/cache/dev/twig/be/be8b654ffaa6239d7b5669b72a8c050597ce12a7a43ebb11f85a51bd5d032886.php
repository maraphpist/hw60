<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_d55ec6d5b6658f9ec0f205de403505345fe05864bfb061d69827ba06242d13bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_535fa354a9e59ba413da0ce0bec39d0b4157bf8375e361a2f7718406efc5b1f0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_535fa354a9e59ba413da0ce0bec39d0b4157bf8375e361a2f7718406efc5b1f0->enter($__internal_535fa354a9e59ba413da0ce0bec39d0b4157bf8375e361a2f7718406efc5b1f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $__internal_1f41bca04bf52eb2f988b8f34b5373f85396b5e2c1e09a4a0eeb8b258d7ad2dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f41bca04bf52eb2f988b8f34b5373f85396b5e2c1e09a4a0eeb8b258d7ad2dc->enter($__internal_1f41bca04bf52eb2f988b8f34b5373f85396b5e2c1e09a4a0eeb8b258d7ad2dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_535fa354a9e59ba413da0ce0bec39d0b4157bf8375e361a2f7718406efc5b1f0->leave($__internal_535fa354a9e59ba413da0ce0bec39d0b4157bf8375e361a2f7718406efc5b1f0_prof);

        
        $__internal_1f41bca04bf52eb2f988b8f34b5373f85396b5e2c1e09a4a0eeb8b258d7ad2dc->leave($__internal_1f41bca04bf52eb2f988b8f34b5373f85396b5e2c1e09a4a0eeb8b258d7ad2dc_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_40061d45a8b791612443b3df0e0148448a38a9dcf8668c06e6c4a2db31a0f466 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40061d45a8b791612443b3df0e0148448a38a9dcf8668c06e6c4a2db31a0f466->enter($__internal_40061d45a8b791612443b3df0e0148448a38a9dcf8668c06e6c4a2db31a0f466_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_6b8e63f4a874fd3fcc9e8dc32febb08ffa83d2143d3137076de7f01f1226b005 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b8e63f4a874fd3fcc9e8dc32febb08ffa83d2143d3137076de7f01f1226b005->enter($__internal_6b8e63f4a874fd3fcc9e8dc32febb08ffa83d2143d3137076de7f01f1226b005_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => ($context["tokenLifetime"] ?? $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_6b8e63f4a874fd3fcc9e8dc32febb08ffa83d2143d3137076de7f01f1226b005->leave($__internal_6b8e63f4a874fd3fcc9e8dc32febb08ffa83d2143d3137076de7f01f1226b005_prof);

        
        $__internal_40061d45a8b791612443b3df0e0148448a38a9dcf8668c06e6c4a2db31a0f466->leave($__internal_40061d45a8b791612443b3df0e0148448a38a9dcf8668c06e6c4a2db31a0f466_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "FOSUserBundle:Resetting:check_email.html.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/check_email.html.twig");
    }
}
