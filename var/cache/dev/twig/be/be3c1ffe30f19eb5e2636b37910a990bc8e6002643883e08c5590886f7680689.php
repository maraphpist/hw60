<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_268d95beb4061e727eea787c5670fa28b7c2b616fc053ccd560ff1dd528c3083 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c1fbd2e17b0b69a205d718273bc1b18b7376283634609a7d435b3257401a04c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c1fbd2e17b0b69a205d718273bc1b18b7376283634609a7d435b3257401a04c6->enter($__internal_c1fbd2e17b0b69a205d718273bc1b18b7376283634609a7d435b3257401a04c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_bc136120ec8734d85cf9eaf6d8b087ed05c16471d1b3bf9d0d18986b08837f1d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc136120ec8734d85cf9eaf6d8b087ed05c16471d1b3bf9d0d18986b08837f1d->enter($__internal_bc136120ec8734d85cf9eaf6d8b087ed05c16471d1b3bf9d0d18986b08837f1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_c1fbd2e17b0b69a205d718273bc1b18b7376283634609a7d435b3257401a04c6->leave($__internal_c1fbd2e17b0b69a205d718273bc1b18b7376283634609a7d435b3257401a04c6_prof);

        
        $__internal_bc136120ec8734d85cf9eaf6d8b087ed05c16471d1b3bf9d0d18986b08837f1d->leave($__internal_bc136120ec8734d85cf9eaf6d8b087ed05c16471d1b3bf9d0d18986b08837f1d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
