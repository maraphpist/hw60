<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_b0585c49b20b01a3d94f4f4e7a6f23972e69e61e6939f7264194d1d425d7a7c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8c6704e682430b1ce25d49e67ccaad0bbb9bcd9e08c260898543b76f1e7a294 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b8c6704e682430b1ce25d49e67ccaad0bbb9bcd9e08c260898543b76f1e7a294->enter($__internal_b8c6704e682430b1ce25d49e67ccaad0bbb9bcd9e08c260898543b76f1e7a294_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_ab5b07b13090bd85fe8d3f152d3a5086a96deecdb3119627a49efa0e191ca33d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab5b07b13090bd85fe8d3f152d3a5086a96deecdb3119627a49efa0e191ca33d->enter($__internal_ab5b07b13090bd85fe8d3f152d3a5086a96deecdb3119627a49efa0e191ca33d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_b8c6704e682430b1ce25d49e67ccaad0bbb9bcd9e08c260898543b76f1e7a294->leave($__internal_b8c6704e682430b1ce25d49e67ccaad0bbb9bcd9e08c260898543b76f1e7a294_prof);

        
        $__internal_ab5b07b13090bd85fe8d3f152d3a5086a96deecdb3119627a49efa0e191ca33d->leave($__internal_ab5b07b13090bd85fe8d3f152d3a5086a96deecdb3119627a49efa0e191ca33d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}
