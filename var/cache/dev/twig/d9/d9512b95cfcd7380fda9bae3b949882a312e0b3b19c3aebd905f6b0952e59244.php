<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_83308ca1b8ae09e2e8878d149b1eb814f2c3872d06b0ece4682fea359edcc633 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_de2edc469770a49e40d1f0b34cf2e1e79c64f3331798bc03e46b7421b4da1205 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_de2edc469770a49e40d1f0b34cf2e1e79c64f3331798bc03e46b7421b4da1205->enter($__internal_de2edc469770a49e40d1f0b34cf2e1e79c64f3331798bc03e46b7421b4da1205_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_ad25418e59c6ca2b2a25788cba9b6d3d21068791d98de64905ec14f74aa37971 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ad25418e59c6ca2b2a25788cba9b6d3d21068791d98de64905ec14f74aa37971->enter($__internal_ad25418e59c6ca2b2a25788cba9b6d3d21068791d98de64905ec14f74aa37971_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_de2edc469770a49e40d1f0b34cf2e1e79c64f3331798bc03e46b7421b4da1205->leave($__internal_de2edc469770a49e40d1f0b34cf2e1e79c64f3331798bc03e46b7421b4da1205_prof);

        
        $__internal_ad25418e59c6ca2b2a25788cba9b6d3d21068791d98de64905ec14f74aa37971->leave($__internal_ad25418e59c6ca2b2a25788cba9b6d3d21068791d98de64905ec14f74aa37971_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_20bd9fd1eec7f3f7fe59fe5be10bb32c27ad88448312b163d3b36bd4f391d4aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_20bd9fd1eec7f3f7fe59fe5be10bb32c27ad88448312b163d3b36bd4f391d4aa->enter($__internal_20bd9fd1eec7f3f7fe59fe5be10bb32c27ad88448312b163d3b36bd4f391d4aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_1243d69ddd1cfeb2021c8ca1d06083ab4108353db1a75fba1e55caba680c12ba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1243d69ddd1cfeb2021c8ca1d06083ab4108353db1a75fba1e55caba680c12ba->enter($__internal_1243d69ddd1cfeb2021c8ca1d06083ab4108353db1a75fba1e55caba680c12ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_1243d69ddd1cfeb2021c8ca1d06083ab4108353db1a75fba1e55caba680c12ba->leave($__internal_1243d69ddd1cfeb2021c8ca1d06083ab4108353db1a75fba1e55caba680c12ba_prof);

        
        $__internal_20bd9fd1eec7f3f7fe59fe5be10bb32c27ad88448312b163d3b36bd4f391d4aa->leave($__internal_20bd9fd1eec7f3f7fe59fe5be10bb32c27ad88448312b163d3b36bd4f391d4aa_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_9a3e9a55a9b502bd4b143657115376b0c42316948054c54134a5d32f2ba00f3d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a3e9a55a9b502bd4b143657115376b0c42316948054c54134a5d32f2ba00f3d->enter($__internal_9a3e9a55a9b502bd4b143657115376b0c42316948054c54134a5d32f2ba00f3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_2fe0c9acc48d489cd2314837f9f061d0ea9ab15a99d84066f1ad1f4c5c96a875 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2fe0c9acc48d489cd2314837f9f061d0ea9ab15a99d84066f1ad1f4c5c96a875->enter($__internal_2fe0c9acc48d489cd2314837f9f061d0ea9ab15a99d84066f1ad1f4c5c96a875_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_2fe0c9acc48d489cd2314837f9f061d0ea9ab15a99d84066f1ad1f4c5c96a875->leave($__internal_2fe0c9acc48d489cd2314837f9f061d0ea9ab15a99d84066f1ad1f4c5c96a875_prof);

        
        $__internal_9a3e9a55a9b502bd4b143657115376b0c42316948054c54134a5d32f2ba00f3d->leave($__internal_9a3e9a55a9b502bd4b143657115376b0c42316948054c54134a5d32f2ba00f3d_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
