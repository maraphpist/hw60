<?php

/* @Framework/Form/widget_container_attributes.html.php */
class __TwigTemplate_6105d531764a47d2abf3d1eb9d52412c37a13bb3b8fe8d06ff3ce497d86df64b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a91eb8da7b543a6f9c904c8cdd9211f6b2aa56d70ed1dffd7846ec52f1d78f7c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a91eb8da7b543a6f9c904c8cdd9211f6b2aa56d70ed1dffd7846ec52f1d78f7c->enter($__internal_a91eb8da7b543a6f9c904c8cdd9211f6b2aa56d70ed1dffd7846ec52f1d78f7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        $__internal_16d69be68fabd20c76584d5c08a5b72db8089362731ce589c5c20b75253b1fcd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16d69be68fabd20c76584d5c08a5b72db8089362731ce589c5c20b75253b1fcd->enter($__internal_16d69be68fabd20c76584d5c08a5b72db8089362731ce589c5c20b75253b1fcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        // line 1
        echo "<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_a91eb8da7b543a6f9c904c8cdd9211f6b2aa56d70ed1dffd7846ec52f1d78f7c->leave($__internal_a91eb8da7b543a6f9c904c8cdd9211f6b2aa56d70ed1dffd7846ec52f1d78f7c_prof);

        
        $__internal_16d69be68fabd20c76584d5c08a5b72db8089362731ce589c5c20b75253b1fcd->leave($__internal_16d69be68fabd20c76584d5c08a5b72db8089362731ce589c5c20b75253b1fcd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_container_attributes.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widget_container_attributes.html.php");
    }
}
