<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class __TwigTemplate_ff745af4c9785908df5f50f3220a611a7a8150c8acb4b2d4fa58002236a15742 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f81f3e5d5adf7f1d8e1727907758d58da7df9b160eb49a77a88e218e2c98c8c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f81f3e5d5adf7f1d8e1727907758d58da7df9b160eb49a77a88e218e2c98c8c->enter($__internal_2f81f3e5d5adf7f1d8e1727907758d58da7df9b160eb49a77a88e218e2c98c8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $__internal_8b1e002a95dbe1c2e7fb83e15a361ee3d8b0fc1c29c6808fb8a8fec3c1d1dc80 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b1e002a95dbe1c2e7fb83e15a361ee3d8b0fc1c29c6808fb8a8fec3c1d1dc80->enter($__internal_8b1e002a95dbe1c2e7fb83e15a361ee3d8b0fc1c29c6808fb8a8fec3c1d1dc80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2f81f3e5d5adf7f1d8e1727907758d58da7df9b160eb49a77a88e218e2c98c8c->leave($__internal_2f81f3e5d5adf7f1d8e1727907758d58da7df9b160eb49a77a88e218e2c98c8c_prof);

        
        $__internal_8b1e002a95dbe1c2e7fb83e15a361ee3d8b0fc1c29c6808fb8a8fec3c1d1dc80->leave($__internal_8b1e002a95dbe1c2e7fb83e15a361ee3d8b0fc1c29c6808fb8a8fec3c1d1dc80_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_61320581576549a8b992a5c540521166c5f687c3c576ad17a4196fbc2b68d972 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_61320581576549a8b992a5c540521166c5f687c3c576ad17a4196fbc2b68d972->enter($__internal_61320581576549a8b992a5c540521166c5f687c3c576ad17a4196fbc2b68d972_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_ada6605661f3905e2e4813ae94310370a274b1c94f4832c366482e907e8cd9d8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ada6605661f3905e2e4813ae94310370a274b1c94f4832c366482e907e8cd9d8->enter($__internal_ada6605661f3905e2e4813ae94310370a274b1c94f4832c366482e907e8cd9d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_ada6605661f3905e2e4813ae94310370a274b1c94f4832c366482e907e8cd9d8->leave($__internal_ada6605661f3905e2e4813ae94310370a274b1c94f4832c366482e907e8cd9d8_prof);

        
        $__internal_61320581576549a8b992a5c540521166c5f687c3c576ad17a4196fbc2b68d972->leave($__internal_61320581576549a8b992a5c540521166c5f687c3c576ad17a4196fbc2b68d972_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_6eb7c808e88e3fa631491424ff4c1c285e3409344ce69bd5271287a6867f566b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6eb7c808e88e3fa631491424ff4c1c285e3409344ce69bd5271287a6867f566b->enter($__internal_6eb7c808e88e3fa631491424ff4c1c285e3409344ce69bd5271287a6867f566b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_c54176e2dbc0b2f30f6a739079acbd499027f24705a20011797404439d3c66d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c54176e2dbc0b2f30f6a739079acbd499027f24705a20011797404439d3c66d0->enter($__internal_c54176e2dbc0b2f30f6a739079acbd499027f24705a20011797404439d3c66d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_c54176e2dbc0b2f30f6a739079acbd499027f24705a20011797404439d3c66d0->leave($__internal_c54176e2dbc0b2f30f6a739079acbd499027f24705a20011797404439d3c66d0_prof);

        
        $__internal_6eb7c808e88e3fa631491424ff4c1c285e3409344ce69bd5271287a6867f566b->leave($__internal_6eb7c808e88e3fa631491424ff4c1c285e3409344ce69bd5271287a6867f566b_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_a3bd876d26fe04198031547945837af321939561791deddd7869647473493c32 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a3bd876d26fe04198031547945837af321939561791deddd7869647473493c32->enter($__internal_a3bd876d26fe04198031547945837af321939561791deddd7869647473493c32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_c10363d0d7d48c11376bfee6e28058af9d60a8ea01a3a8cb60abc324390aced4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c10363d0d7d48c11376bfee6e28058af9d60a8ea01a3a8cb60abc324390aced4->enter($__internal_c10363d0d7d48c11376bfee6e28058af9d60a8ea01a3a8cb60abc324390aced4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_c10363d0d7d48c11376bfee6e28058af9d60a8ea01a3a8cb60abc324390aced4->leave($__internal_c10363d0d7d48c11376bfee6e28058af9d60a8ea01a3a8cb60abc324390aced4_prof);

        
        $__internal_a3bd876d26fe04198031547945837af321939561791deddd7869647473493c32->leave($__internal_a3bd876d26fe04198031547945837af321939561791deddd7869647473493c32_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "WebProfilerBundle:Collector:exception.html.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
