<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_67ca4283ac4c513189ba611cf0e68525ccf5da3ed53f5d19ce9451d14d61a2bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c536488b6f63cc03cd8765515cb5d2d07f1f0e501eb67ffa73a74f578a8237b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c536488b6f63cc03cd8765515cb5d2d07f1f0e501eb67ffa73a74f578a8237b2->enter($__internal_c536488b6f63cc03cd8765515cb5d2d07f1f0e501eb67ffa73a74f578a8237b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_dd080c94f530e8483933cea7e1eac79c006a4628cde9a7e27a9e836140786df3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd080c94f530e8483933cea7e1eac79c006a4628cde9a7e27a9e836140786df3->enter($__internal_dd080c94f530e8483933cea7e1eac79c006a4628cde9a7e27a9e836140786df3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_c536488b6f63cc03cd8765515cb5d2d07f1f0e501eb67ffa73a74f578a8237b2->leave($__internal_c536488b6f63cc03cd8765515cb5d2d07f1f0e501eb67ffa73a74f578a8237b2_prof);

        
        $__internal_dd080c94f530e8483933cea7e1eac79c006a4628cde9a7e27a9e836140786df3->leave($__internal_dd080c94f530e8483933cea7e1eac79c006a4628cde9a7e27a9e836140786df3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}
