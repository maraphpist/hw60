<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_2513e19d2d13d7bbf5d6f912b3544f49838f36e56415939132eddc3ef29bc2ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0f1118162630cc15cb5448f51e0db6e63e1b68d36ed4a458585b709c1a24d4fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0f1118162630cc15cb5448f51e0db6e63e1b68d36ed4a458585b709c1a24d4fb->enter($__internal_0f1118162630cc15cb5448f51e0db6e63e1b68d36ed4a458585b709c1a24d4fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        $__internal_521d987edf1da92efef5344bfdecd649aa5462ff316572316a0ce4a23c3d834d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_521d987edf1da92efef5344bfdecd649aa5462ff316572316a0ce4a23c3d834d->enter($__internal_521d987edf1da92efef5344bfdecd649aa5462ff316572316a0ce4a23c3d834d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_0f1118162630cc15cb5448f51e0db6e63e1b68d36ed4a458585b709c1a24d4fb->leave($__internal_0f1118162630cc15cb5448f51e0db6e63e1b68d36ed4a458585b709c1a24d4fb_prof);

        
        $__internal_521d987edf1da92efef5344bfdecd649aa5462ff316572316a0ce4a23c3d834d->leave($__internal_521d987edf1da92efef5344bfdecd649aa5462ff316572316a0ce4a23c3d834d_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_fd4c980e107da4305670c6daba4e826d626c83c0c504962d339b2fc608d80065 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd4c980e107da4305670c6daba4e826d626c83c0c504962d339b2fc608d80065->enter($__internal_fd4c980e107da4305670c6daba4e826d626c83c0c504962d339b2fc608d80065_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_172829fae63f48d0105378f25bd0f01f3f6cade8ee440fed76fa9a18231e22e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_172829fae63f48d0105378f25bd0f01f3f6cade8ee440fed76fa9a18231e22e7->enter($__internal_172829fae63f48d0105378f25bd0f01f3f6cade8ee440fed76fa9a18231e22e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_172829fae63f48d0105378f25bd0f01f3f6cade8ee440fed76fa9a18231e22e7->leave($__internal_172829fae63f48d0105378f25bd0f01f3f6cade8ee440fed76fa9a18231e22e7_prof);

        
        $__internal_fd4c980e107da4305670c6daba4e826d626c83c0c504962d339b2fc608d80065->leave($__internal_fd4c980e107da4305670c6daba4e826d626c83c0c504962d339b2fc608d80065_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_6efc12019679bff59f3cac547161d0317ced55b0844c6c799027489805483aea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6efc12019679bff59f3cac547161d0317ced55b0844c6c799027489805483aea->enter($__internal_6efc12019679bff59f3cac547161d0317ced55b0844c6c799027489805483aea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_cfcd5473acf538cf8091a0d671e7fb9c2540d9566f43b7d35b12c93500977ce2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cfcd5473acf538cf8091a0d671e7fb9c2540d9566f43b7d35b12c93500977ce2->enter($__internal_cfcd5473acf538cf8091a0d671e7fb9c2540d9566f43b7d35b12c93500977ce2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_cfcd5473acf538cf8091a0d671e7fb9c2540d9566f43b7d35b12c93500977ce2->leave($__internal_cfcd5473acf538cf8091a0d671e7fb9c2540d9566f43b7d35b12c93500977ce2_prof);

        
        $__internal_6efc12019679bff59f3cac547161d0317ced55b0844c6c799027489805483aea->leave($__internal_6efc12019679bff59f3cac547161d0317ced55b0844c6c799027489805483aea_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_a981730b619dac0dfa85212c6dab36c77c0b671ab7701ed3ba7dfa5786c35a5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a981730b619dac0dfa85212c6dab36c77c0b671ab7701ed3ba7dfa5786c35a5a->enter($__internal_a981730b619dac0dfa85212c6dab36c77c0b671ab7701ed3ba7dfa5786c35a5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_cdd8066e9d9692c9de9ce416d379a40254a980b5594623d2b433c68f80fbc16b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cdd8066e9d9692c9de9ce416d379a40254a980b5594623d2b433c68f80fbc16b->enter($__internal_cdd8066e9d9692c9de9ce416d379a40254a980b5594623d2b433c68f80fbc16b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_cdd8066e9d9692c9de9ce416d379a40254a980b5594623d2b433c68f80fbc16b->leave($__internal_cdd8066e9d9692c9de9ce416d379a40254a980b5594623d2b433c68f80fbc16b_prof);

        
        $__internal_a981730b619dac0dfa85212c6dab36c77c0b671ab7701ed3ba7dfa5786c35a5a->leave($__internal_a981730b619dac0dfa85212c6dab36c77c0b671ab7701ed3ba7dfa5786c35a5a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/email.txt.twig");
    }
}
