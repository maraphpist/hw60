<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_62ace0ccecacfdc7c1b686bc690b2519e2504a94b53e1e84759afb9c2465d7d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c54d06373cb803dbabbbdc64a539d31c8323bd80cc7533723ba9b9da2a5d0ea2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c54d06373cb803dbabbbdc64a539d31c8323bd80cc7533723ba9b9da2a5d0ea2->enter($__internal_c54d06373cb803dbabbbdc64a539d31c8323bd80cc7533723ba9b9da2a5d0ea2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $__internal_ca0678c710ec750ea4839ecfd2ee9217f0c4a2e4d6c94030f6976085b5016648 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca0678c710ec750ea4839ecfd2ee9217f0c4a2e4d6c94030f6976085b5016648->enter($__internal_ca0678c710ec750ea4839ecfd2ee9217f0c4a2e4d6c94030f6976085b5016648_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c54d06373cb803dbabbbdc64a539d31c8323bd80cc7533723ba9b9da2a5d0ea2->leave($__internal_c54d06373cb803dbabbbdc64a539d31c8323bd80cc7533723ba9b9da2a5d0ea2_prof);

        
        $__internal_ca0678c710ec750ea4839ecfd2ee9217f0c4a2e4d6c94030f6976085b5016648->leave($__internal_ca0678c710ec750ea4839ecfd2ee9217f0c4a2e4d6c94030f6976085b5016648_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ae867b5e271e8a588029b47299ce3f03b7f634f5ac86870148ead3f46ac14853 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae867b5e271e8a588029b47299ce3f03b7f634f5ac86870148ead3f46ac14853->enter($__internal_ae867b5e271e8a588029b47299ce3f03b7f634f5ac86870148ead3f46ac14853_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_501ec2e76d50e111ee46b708de8ec7c4b33f5d9f14b89fe90f2ee7d59a31458e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_501ec2e76d50e111ee46b708de8ec7c4b33f5d9f14b89fe90f2ee7d59a31458e->enter($__internal_501ec2e76d50e111ee46b708de8ec7c4b33f5d9f14b89fe90f2ee7d59a31458e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_501ec2e76d50e111ee46b708de8ec7c4b33f5d9f14b89fe90f2ee7d59a31458e->leave($__internal_501ec2e76d50e111ee46b708de8ec7c4b33f5d9f14b89fe90f2ee7d59a31458e_prof);

        
        $__internal_ae867b5e271e8a588029b47299ce3f03b7f634f5ac86870148ead3f46ac14853->leave($__internal_ae867b5e271e8a588029b47299ce3f03b7f634f5ac86870148ead3f46ac14853_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:new.html.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new.html.twig");
    }
}
