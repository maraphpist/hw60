<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_6be2fa8c026f231cc0acc0b18d7daeaf8f7b74e8b59c58c4285bb6535f71616a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a925d849000be1cb56bf77f0faa481e164036a206e7b956d8ac6fd74a18506ff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a925d849000be1cb56bf77f0faa481e164036a206e7b956d8ac6fd74a18506ff->enter($__internal_a925d849000be1cb56bf77f0faa481e164036a206e7b956d8ac6fd74a18506ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        $__internal_f1ea06d0dc7b98cff1f5bad878d9e6bd8ed33b6cab2c7908777516ce16346d65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f1ea06d0dc7b98cff1f5bad878d9e6bd8ed33b6cab2c7908777516ce16346d65->enter($__internal_f1ea06d0dc7b98cff1f5bad878d9e6bd8ed33b6cab2c7908777516ce16346d65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_a925d849000be1cb56bf77f0faa481e164036a206e7b956d8ac6fd74a18506ff->leave($__internal_a925d849000be1cb56bf77f0faa481e164036a206e7b956d8ac6fd74a18506ff_prof);

        
        $__internal_f1ea06d0dc7b98cff1f5bad878d9e6bd8ed33b6cab2c7908777516ce16346d65->leave($__internal_f1ea06d0dc7b98cff1f5bad878d9e6bd8ed33b6cab2c7908777516ce16346d65_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_359ada4742376ecc1f45bee107fa68459593362a7e34bb89a2e076ffb6a77b91 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_359ada4742376ecc1f45bee107fa68459593362a7e34bb89a2e076ffb6a77b91->enter($__internal_359ada4742376ecc1f45bee107fa68459593362a7e34bb89a2e076ffb6a77b91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_385849070acb190df95fa2d75c873034f78dae8e91c51f6ace8c7752445cc2fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_385849070acb190df95fa2d75c873034f78dae8e91c51f6ace8c7752445cc2fc->enter($__internal_385849070acb190df95fa2d75c873034f78dae8e91c51f6ace8c7752445cc2fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_385849070acb190df95fa2d75c873034f78dae8e91c51f6ace8c7752445cc2fc->leave($__internal_385849070acb190df95fa2d75c873034f78dae8e91c51f6ace8c7752445cc2fc_prof);

        
        $__internal_359ada4742376ecc1f45bee107fa68459593362a7e34bb89a2e076ffb6a77b91->leave($__internal_359ada4742376ecc1f45bee107fa68459593362a7e34bb89a2e076ffb6a77b91_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_2945856c18a938c38ad1b85f2029240a235dd3fb054f14a84f46a3140130297b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2945856c18a938c38ad1b85f2029240a235dd3fb054f14a84f46a3140130297b->enter($__internal_2945856c18a938c38ad1b85f2029240a235dd3fb054f14a84f46a3140130297b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_16e2ab23e5c00557f560e2ad005242c96b1a49aa88e78d72f75d87b4470033f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16e2ab23e5c00557f560e2ad005242c96b1a49aa88e78d72f75d87b4470033f0->enter($__internal_16e2ab23e5c00557f560e2ad005242c96b1a49aa88e78d72f75d87b4470033f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_16e2ab23e5c00557f560e2ad005242c96b1a49aa88e78d72f75d87b4470033f0->leave($__internal_16e2ab23e5c00557f560e2ad005242c96b1a49aa88e78d72f75d87b4470033f0_prof);

        
        $__internal_2945856c18a938c38ad1b85f2029240a235dd3fb054f14a84f46a3140130297b->leave($__internal_2945856c18a938c38ad1b85f2029240a235dd3fb054f14a84f46a3140130297b_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_394cd9c2a7b9fce220781710582e0bb31ce81c1cb92461cfc49dff1987a3ed81 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_394cd9c2a7b9fce220781710582e0bb31ce81c1cb92461cfc49dff1987a3ed81->enter($__internal_394cd9c2a7b9fce220781710582e0bb31ce81c1cb92461cfc49dff1987a3ed81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_6e500db938faf83c46f48ed15baab4f3634192ac9a7a2156ad8cfe9496d40562 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e500db938faf83c46f48ed15baab4f3634192ac9a7a2156ad8cfe9496d40562->enter($__internal_6e500db938faf83c46f48ed15baab4f3634192ac9a7a2156ad8cfe9496d40562_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_6e500db938faf83c46f48ed15baab4f3634192ac9a7a2156ad8cfe9496d40562->leave($__internal_6e500db938faf83c46f48ed15baab4f3634192ac9a7a2156ad8cfe9496d40562_prof);

        
        $__internal_394cd9c2a7b9fce220781710582e0bb31ce81c1cb92461cfc49dff1987a3ed81->leave($__internal_394cd9c2a7b9fce220781710582e0bb31ce81c1cb92461cfc49dff1987a3ed81_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}
