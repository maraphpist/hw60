<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_67bc54a9cfb341627c78f590329a16538b5900722b5ff93d79e4a1eba52725cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f005781e3f5faf656a7f9ce07745ecf0f420cf3603eebdaf0f2591d7a76ab557 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f005781e3f5faf656a7f9ce07745ecf0f420cf3603eebdaf0f2591d7a76ab557->enter($__internal_f005781e3f5faf656a7f9ce07745ecf0f420cf3603eebdaf0f2591d7a76ab557_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        $__internal_c8a30651fa1deebcf44b9f037e83d706547d52009891a953b1b613ad47ff4376 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8a30651fa1deebcf44b9f037e83d706547d52009891a953b1b613ad47ff4376->enter($__internal_c8a30651fa1deebcf44b9f037e83d706547d52009891a953b1b613ad47ff4376_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, ($context["widget"] ?? $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_f005781e3f5faf656a7f9ce07745ecf0f420cf3603eebdaf0f2591d7a76ab557->leave($__internal_f005781e3f5faf656a7f9ce07745ecf0f420cf3603eebdaf0f2591d7a76ab557_prof);

        
        $__internal_c8a30651fa1deebcf44b9f037e83d706547d52009891a953b1b613ad47ff4376->leave($__internal_c8a30651fa1deebcf44b9f037e83d706547d52009891a953b1b613ad47ff4376_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo str_replace('{{ widget }}', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
", "@Framework/Form/money_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/money_widget.html.php");
    }
}
