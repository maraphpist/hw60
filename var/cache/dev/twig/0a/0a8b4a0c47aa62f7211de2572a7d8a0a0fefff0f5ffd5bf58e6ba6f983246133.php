<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_77cb403ac9f5d693785a8bf26efa5d431e95dcc39820057b02a197d57f751ce0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b3731556ab4a9e0c19fcf3f2a92a31dee6c8b4ea5bb4deea526ca1c3699fcdc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7b3731556ab4a9e0c19fcf3f2a92a31dee6c8b4ea5bb4deea526ca1c3699fcdc->enter($__internal_7b3731556ab4a9e0c19fcf3f2a92a31dee6c8b4ea5bb4deea526ca1c3699fcdc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_20f0e44757cf9820c3b3fbb5e15fd58bb9c8fc5eb131a98cc11d64634e6a2e6e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20f0e44757cf9820c3b3fbb5e15fd58bb9c8fc5eb131a98cc11d64634e6a2e6e->enter($__internal_20f0e44757cf9820c3b3fbb5e15fd58bb9c8fc5eb131a98cc11d64634e6a2e6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_7b3731556ab4a9e0c19fcf3f2a92a31dee6c8b4ea5bb4deea526ca1c3699fcdc->leave($__internal_7b3731556ab4a9e0c19fcf3f2a92a31dee6c8b4ea5bb4deea526ca1c3699fcdc_prof);

        
        $__internal_20f0e44757cf9820c3b3fbb5e15fd58bb9c8fc5eb131a98cc11d64634e6a2e6e->leave($__internal_20f0e44757cf9820c3b3fbb5e15fd58bb9c8fc5eb131a98cc11d64634e6a2e6e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
