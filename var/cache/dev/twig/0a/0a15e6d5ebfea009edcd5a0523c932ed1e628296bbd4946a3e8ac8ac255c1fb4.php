<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_7bd36af570d7e8e3cd6332c5b810750aae9bc5356b7a1fc7226b578b8debba57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b148fc62e6c444ec5c47b671e8d1971a11c4505b9281afc4e6a1acaf8d345474 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b148fc62e6c444ec5c47b671e8d1971a11c4505b9281afc4e6a1acaf8d345474->enter($__internal_b148fc62e6c444ec5c47b671e8d1971a11c4505b9281afc4e6a1acaf8d345474_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        $__internal_0ac2d1cf7ccdc16236e07a680caf549e60ee18cf42aad3e18e62c5c4f930e026 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0ac2d1cf7ccdc16236e07a680caf549e60ee18cf42aad3e18e62c5c4f930e026->enter($__internal_0ac2d1cf7ccdc16236e07a680caf549e60ee18cf42aad3e18e62c5c4f930e026_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_b148fc62e6c444ec5c47b671e8d1971a11c4505b9281afc4e6a1acaf8d345474->leave($__internal_b148fc62e6c444ec5c47b671e8d1971a11c4505b9281afc4e6a1acaf8d345474_prof);

        
        $__internal_0ac2d1cf7ccdc16236e07a680caf549e60ee18cf42aad3e18e62c5c4f930e026->leave($__internal_0ac2d1cf7ccdc16236e07a680caf549e60ee18cf42aad3e18e62c5c4f930e026_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.atom.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}
