<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_17ba7283a10a58f663a8c9cfa9a16567887e0ac21fd5fa929fb362fd4db8effa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_84e2a02fcff6186ccf81058a153d060e1763a7b20999f5a747702c36d911478a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_84e2a02fcff6186ccf81058a153d060e1763a7b20999f5a747702c36d911478a->enter($__internal_84e2a02fcff6186ccf81058a153d060e1763a7b20999f5a747702c36d911478a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_435a033bc5f66b92b43f8d58fd9af5fddb846a604619d579b8570f138e95cf93 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_435a033bc5f66b92b43f8d58fd9af5fddb846a604619d579b8570f138e95cf93->enter($__internal_435a033bc5f66b92b43f8d58fd9af5fddb846a604619d579b8570f138e95cf93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_84e2a02fcff6186ccf81058a153d060e1763a7b20999f5a747702c36d911478a->leave($__internal_84e2a02fcff6186ccf81058a153d060e1763a7b20999f5a747702c36d911478a_prof);

        
        $__internal_435a033bc5f66b92b43f8d58fd9af5fddb846a604619d579b8570f138e95cf93->leave($__internal_435a033bc5f66b92b43f8d58fd9af5fddb846a604619d579b8570f138e95cf93_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
