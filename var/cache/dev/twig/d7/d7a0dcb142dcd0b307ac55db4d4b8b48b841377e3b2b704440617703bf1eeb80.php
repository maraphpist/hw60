<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_ba4eeaddd5d43926e9828b0f443f7b3eb6428665e77b8be38e5fb4fe681b7168 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e34796f6935a0f733f0b178accccef021ed4338942b57957ee2efed0ab0e594 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e34796f6935a0f733f0b178accccef021ed4338942b57957ee2efed0ab0e594->enter($__internal_7e34796f6935a0f733f0b178accccef021ed4338942b57957ee2efed0ab0e594_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_b0764430a7394a2cf3fc8966eba5d65a677f1fcf9e3849eff73dcc8f203a3ada = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b0764430a7394a2cf3fc8966eba5d65a677f1fcf9e3849eff73dcc8f203a3ada->enter($__internal_b0764430a7394a2cf3fc8966eba5d65a677f1fcf9e3849eff73dcc8f203a3ada_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_7e34796f6935a0f733f0b178accccef021ed4338942b57957ee2efed0ab0e594->leave($__internal_7e34796f6935a0f733f0b178accccef021ed4338942b57957ee2efed0ab0e594_prof);

        
        $__internal_b0764430a7394a2cf3fc8966eba5d65a677f1fcf9e3849eff73dcc8f203a3ada->leave($__internal_b0764430a7394a2cf3fc8966eba5d65a677f1fcf9e3849eff73dcc8f203a3ada_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}
