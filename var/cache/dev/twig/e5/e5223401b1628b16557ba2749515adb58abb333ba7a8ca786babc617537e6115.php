<?php

/* TwigBundle:Exception:traces.xml.twig */
class __TwigTemplate_68652115a08ba0780d2ce94592741b04a4d45329bd0174232febbe43f1906fd8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1794700e72f9c617077fcf90da5f4979d5a4f56d1db5b2caa9cb7cdc9759dda8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1794700e72f9c617077fcf90da5f4979d5a4f56d1db5b2caa9cb7cdc9759dda8->enter($__internal_1794700e72f9c617077fcf90da5f4979d5a4f56d1db5b2caa9cb7cdc9759dda8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        $__internal_3cce7496443b6adf72927eb2d1b91d0e8f6610fa768f9470d32fadca12957273 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3cce7496443b6adf72927eb2d1b91d0e8f6610fa768f9470d32fadca12957273->enter($__internal_3cce7496443b6adf72927eb2d1b91d0e8f6610fa768f9470d32fadca12957273_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        // line 1
        echo "        <traces>
";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
            // line 3
            echo "            <trace>
";
            // line 4
            echo twig_include($this->env, $context, "@Twig/Exception/trace.txt.twig", array("trace" => $context["trace"]), false);
            echo "

            </trace>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "        </traces>
";
        
        $__internal_1794700e72f9c617077fcf90da5f4979d5a4f56d1db5b2caa9cb7cdc9759dda8->leave($__internal_1794700e72f9c617077fcf90da5f4979d5a4f56d1db5b2caa9cb7cdc9759dda8_prof);

        
        $__internal_3cce7496443b6adf72927eb2d1b91d0e8f6610fa768f9470d32fadca12957273->leave($__internal_3cce7496443b6adf72927eb2d1b91d0e8f6610fa768f9470d32fadca12957273_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 8,  35 => 4,  32 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("        <traces>
{% for trace in exception.trace %}
            <trace>
{{ include('@Twig/Exception/trace.txt.twig', { trace: trace }, with_context = false) }}

            </trace>
{% endfor %}
        </traces>
", "TwigBundle:Exception:traces.xml.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.xml.twig");
    }
}
