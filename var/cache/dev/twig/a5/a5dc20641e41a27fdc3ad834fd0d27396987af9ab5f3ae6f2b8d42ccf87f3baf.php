<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_73df4408d4afd78de1a8195505139769de9e3d178bbd706eaf09d5975e7b1cfe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d496b4b7f90a0b9fffda805d17a0da6f6b8c3312396f2a8297e060897586337 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d496b4b7f90a0b9fffda805d17a0da6f6b8c3312396f2a8297e060897586337->enter($__internal_5d496b4b7f90a0b9fffda805d17a0da6f6b8c3312396f2a8297e060897586337_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_d78ddebc0575c4dadd46f390b95dac63f834003e7dd609b01de59041cd6433d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d78ddebc0575c4dadd46f390b95dac63f834003e7dd609b01de59041cd6433d6->enter($__internal_d78ddebc0575c4dadd46f390b95dac63f834003e7dd609b01de59041cd6433d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_5d496b4b7f90a0b9fffda805d17a0da6f6b8c3312396f2a8297e060897586337->leave($__internal_5d496b4b7f90a0b9fffda805d17a0da6f6b8c3312396f2a8297e060897586337_prof);

        
        $__internal_d78ddebc0575c4dadd46f390b95dac63f834003e7dd609b01de59041cd6433d6->leave($__internal_d78ddebc0575c4dadd46f390b95dac63f834003e7dd609b01de59041cd6433d6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}
