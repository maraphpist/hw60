<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_9ec924366e2f87664ba94044a4897bf66d2b63a663b41eee1f997752f7076340 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_151f0acf910991f65535910fe5abf599e5cf9663ce424d11b915d1e450223fde = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_151f0acf910991f65535910fe5abf599e5cf9663ce424d11b915d1e450223fde->enter($__internal_151f0acf910991f65535910fe5abf599e5cf9663ce424d11b915d1e450223fde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_ca1df5d637d62e0b4f54714029d0262a677c09219c4047fd4a2c3abfb77797a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca1df5d637d62e0b4f54714029d0262a677c09219c4047fd4a2c3abfb77797a7->enter($__internal_ca1df5d637d62e0b4f54714029d0262a677c09219c4047fd4a2c3abfb77797a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_151f0acf910991f65535910fe5abf599e5cf9663ce424d11b915d1e450223fde->leave($__internal_151f0acf910991f65535910fe5abf599e5cf9663ce424d11b915d1e450223fde_prof);

        
        $__internal_ca1df5d637d62e0b4f54714029d0262a677c09219c4047fd4a2c3abfb77797a7->leave($__internal_ca1df5d637d62e0b4f54714029d0262a677c09219c4047fd4a2c3abfb77797a7_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.js.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
