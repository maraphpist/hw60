<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_14ee7c3912401387de17c56ebfb16063f7708920acbbc5432689de80cfcc7df9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e1f3bfb8ad6b505405faa7fc4ca20d8a32d7425f3ce166100b9ec042bdb9034c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e1f3bfb8ad6b505405faa7fc4ca20d8a32d7425f3ce166100b9ec042bdb9034c->enter($__internal_e1f3bfb8ad6b505405faa7fc4ca20d8a32d7425f3ce166100b9ec042bdb9034c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_bda3c023c3e6b81255f7dde4d6f5663db97ad3785780b9c9ee6c5a0fce930c5d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bda3c023c3e6b81255f7dde4d6f5663db97ad3785780b9c9ee6c5a0fce930c5d->enter($__internal_bda3c023c3e6b81255f7dde4d6f5663db97ad3785780b9c9ee6c5a0fce930c5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_e1f3bfb8ad6b505405faa7fc4ca20d8a32d7425f3ce166100b9ec042bdb9034c->leave($__internal_e1f3bfb8ad6b505405faa7fc4ca20d8a32d7425f3ce166100b9ec042bdb9034c_prof);

        
        $__internal_bda3c023c3e6b81255f7dde4d6f5663db97ad3785780b9c9ee6c5a0fce930c5d->leave($__internal_bda3c023c3e6b81255f7dde4d6f5663db97ad3785780b9c9ee6c5a0fce930c5d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}
