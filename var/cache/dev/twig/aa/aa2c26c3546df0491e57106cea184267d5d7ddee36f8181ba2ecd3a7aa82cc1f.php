<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_44e449d7e286c49bde1eb2722d0db18bc1a44e15941931a92381e123f94f0614 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1eb63e177f3aa1cd1f89e7dc9ecd0a45d4a3e585d6129f2c7aa08993f099af8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1eb63e177f3aa1cd1f89e7dc9ecd0a45d4a3e585d6129f2c7aa08993f099af8b->enter($__internal_1eb63e177f3aa1cd1f89e7dc9ecd0a45d4a3e585d6129f2c7aa08993f099af8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_933261ab67c2136d7ccf43df2e0d29bc435a5a843b0a9e047e40cf0b7ab26877 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_933261ab67c2136d7ccf43df2e0d29bc435a5a843b0a9e047e40cf0b7ab26877->enter($__internal_933261ab67c2136d7ccf43df2e0d29bc435a5a843b0a9e047e40cf0b7ab26877_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_1eb63e177f3aa1cd1f89e7dc9ecd0a45d4a3e585d6129f2c7aa08993f099af8b->leave($__internal_1eb63e177f3aa1cd1f89e7dc9ecd0a45d4a3e585d6129f2c7aa08993f099af8b_prof);

        
        $__internal_933261ab67c2136d7ccf43df2e0d29bc435a5a843b0a9e047e40cf0b7ab26877->leave($__internal_933261ab67c2136d7ccf43df2e0d29bc435a5a843b0a9e047e40cf0b7ab26877_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}
