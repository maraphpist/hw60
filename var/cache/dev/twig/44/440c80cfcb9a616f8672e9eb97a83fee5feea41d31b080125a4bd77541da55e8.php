<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_aa31c7e191ff782a3a5468e162637150d42504414a35e2c448f799d93d4b86d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2394e9caf77dad22825c1526abc573580d274c308f9d4bdde4eecef8b807870d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2394e9caf77dad22825c1526abc573580d274c308f9d4bdde4eecef8b807870d->enter($__internal_2394e9caf77dad22825c1526abc573580d274c308f9d4bdde4eecef8b807870d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        $__internal_3873983b1064e64f71804e674198f231f91ffcbff7f23e2c4e0943332f6bb7f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3873983b1064e64f71804e674198f231f91ffcbff7f23e2c4e0943332f6bb7f0->enter($__internal_3873983b1064e64f71804e674198f231f91ffcbff7f23e2c4e0943332f6bb7f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_2394e9caf77dad22825c1526abc573580d274c308f9d4bdde4eecef8b807870d->leave($__internal_2394e9caf77dad22825c1526abc573580d274c308f9d4bdde4eecef8b807870d_prof);

        
        $__internal_3873983b1064e64f71804e674198f231f91ffcbff7f23e2c4e0943332f6bb7f0->leave($__internal_3873983b1064e64f71804e674198f231f91ffcbff7f23e2c4e0943332f6bb7f0_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.atom.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}
