<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_89612bd6c62318aa241fa727dd099b5497e535db6ead2ad52446c47f14020461 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_898a499663ccf438cb8c85559a02ab6148d430ae11d855cfc764d3a31e624bf7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_898a499663ccf438cb8c85559a02ab6148d430ae11d855cfc764d3a31e624bf7->enter($__internal_898a499663ccf438cb8c85559a02ab6148d430ae11d855cfc764d3a31e624bf7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_41a3ad01b814273ed80b440211f3f0e70862896c306ab7c41480fc8ebf38ffef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41a3ad01b814273ed80b440211f3f0e70862896c306ab7c41480fc8ebf38ffef->enter($__internal_41a3ad01b814273ed80b440211f3f0e70862896c306ab7c41480fc8ebf38ffef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_898a499663ccf438cb8c85559a02ab6148d430ae11d855cfc764d3a31e624bf7->leave($__internal_898a499663ccf438cb8c85559a02ab6148d430ae11d855cfc764d3a31e624bf7_prof);

        
        $__internal_41a3ad01b814273ed80b440211f3f0e70862896c306ab7c41480fc8ebf38ffef->leave($__internal_41a3ad01b814273ed80b440211f3f0e70862896c306ab7c41480fc8ebf38ffef_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datetime_widget.html.php");
    }
}
