<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_c7a3586c769d8ac8fbeb9126643e286decb1acada38f88a003d0ee18bd3bacc9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0de7b89a6b5287552685554522aaf5dffb2f61649a65732b56a43a8862957502 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0de7b89a6b5287552685554522aaf5dffb2f61649a65732b56a43a8862957502->enter($__internal_0de7b89a6b5287552685554522aaf5dffb2f61649a65732b56a43a8862957502_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_ee250c5e6820979e17766fbd16f7582e6f74a1133ed53678f3bd5c2e8bfff9dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ee250c5e6820979e17766fbd16f7582e6f74a1133ed53678f3bd5c2e8bfff9dc->enter($__internal_ee250c5e6820979e17766fbd16f7582e6f74a1133ed53678f3bd5c2e8bfff9dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_0de7b89a6b5287552685554522aaf5dffb2f61649a65732b56a43a8862957502->leave($__internal_0de7b89a6b5287552685554522aaf5dffb2f61649a65732b56a43a8862957502_prof);

        
        $__internal_ee250c5e6820979e17766fbd16f7582e6f74a1133ed53678f3bd5c2e8bfff9dc->leave($__internal_ee250c5e6820979e17766fbd16f7582e6f74a1133ed53678f3bd5c2e8bfff9dc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_row.html.php");
    }
}
