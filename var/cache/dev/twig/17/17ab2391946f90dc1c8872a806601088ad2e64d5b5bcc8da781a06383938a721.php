<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_e12ba50100984e4be16524ff0b359558caf5114763600052ebc819e4740ad228 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4bbfa9a4001ef71b15468d7ebb9a7ca50c31cee9ab536853116c449f82e44394 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4bbfa9a4001ef71b15468d7ebb9a7ca50c31cee9ab536853116c449f82e44394->enter($__internal_4bbfa9a4001ef71b15468d7ebb9a7ca50c31cee9ab536853116c449f82e44394_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_d99080a4b451dada6c66d9e2fbf0b33dedddc336716a5a27b2516c7d54a3226a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d99080a4b451dada6c66d9e2fbf0b33dedddc336716a5a27b2516c7d54a3226a->enter($__internal_d99080a4b451dada6c66d9e2fbf0b33dedddc336716a5a27b2516c7d54a3226a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_4bbfa9a4001ef71b15468d7ebb9a7ca50c31cee9ab536853116c449f82e44394->leave($__internal_4bbfa9a4001ef71b15468d7ebb9a7ca50c31cee9ab536853116c449f82e44394_prof);

        
        $__internal_d99080a4b451dada6c66d9e2fbf0b33dedddc336716a5a27b2516c7d54a3226a->leave($__internal_d99080a4b451dada6c66d9e2fbf0b33dedddc336716a5a27b2516c7d54a3226a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
