<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_11e5710c6af86f6f6ce133a04312d2493e47fd7fe737533a5296fb25c92db98b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0bbde164540ca2fb5b8b0be8b1bf372df908aa52427e89694c3ccc616f81f0f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0bbde164540ca2fb5b8b0be8b1bf372df908aa52427e89694c3ccc616f81f0f9->enter($__internal_0bbde164540ca2fb5b8b0be8b1bf372df908aa52427e89694c3ccc616f81f0f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_c30391801b5c84a71fea80df958dcfcc7c26945623fbaa566d53696c907ec5ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c30391801b5c84a71fea80df958dcfcc7c26945623fbaa566d53696c907ec5ad->enter($__internal_c30391801b5c84a71fea80df958dcfcc7c26945623fbaa566d53696c907ec5ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_0bbde164540ca2fb5b8b0be8b1bf372df908aa52427e89694c3ccc616f81f0f9->leave($__internal_0bbde164540ca2fb5b8b0be8b1bf372df908aa52427e89694c3ccc616f81f0f9_prof);

        
        $__internal_c30391801b5c84a71fea80df958dcfcc7c26945623fbaa566d53696c907ec5ad->leave($__internal_c30391801b5c84a71fea80df958dcfcc7c26945623fbaa566d53696c907ec5ad_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.css.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}
