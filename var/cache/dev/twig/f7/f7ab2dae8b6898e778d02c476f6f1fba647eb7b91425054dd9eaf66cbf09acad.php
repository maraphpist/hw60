<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_8b1fcd48a07dcdfd821f09be903353aec670db3a488b39c395e01584411e7555 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7c1f3660c52c7fef14dbead86f21ec30e65f80623d49e7ab981d0f80675469c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c1f3660c52c7fef14dbead86f21ec30e65f80623d49e7ab981d0f80675469c8->enter($__internal_7c1f3660c52c7fef14dbead86f21ec30e65f80623d49e7ab981d0f80675469c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        $__internal_6f5c996bea2722aacfc4aedac96016316bc056e5d5f3aa3ac34efca799492597 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f5c996bea2722aacfc4aedac96016316bc056e5d5f3aa3ac34efca799492597->enter($__internal_6f5c996bea2722aacfc4aedac96016316bc056e5d5f3aa3ac34efca799492597_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_7c1f3660c52c7fef14dbead86f21ec30e65f80623d49e7ab981d0f80675469c8->leave($__internal_7c1f3660c52c7fef14dbead86f21ec30e65f80623d49e7ab981d0f80675469c8_prof);

        
        $__internal_6f5c996bea2722aacfc4aedac96016316bc056e5d5f3aa3ac34efca799492597->leave($__internal_6f5c996bea2722aacfc4aedac96016316bc056e5d5f3aa3ac34efca799492597_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?xml version=\"1.0\" encoding=\"{{ _charset }}\" ?>

<error code=\"{{ status_code }}\" message=\"{{ status_text }}\" />
", "TwigBundle:Exception:error.xml.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.xml.twig");
    }
}
