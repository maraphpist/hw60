<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_289d0ec649af2677564e75fc0a745ef9ecf3d4d5cc0ff2792a47adf19c2b495f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a7b1de1ca1e7f875ae483c27efb1ef4b7c3f45f2b27b040a534f5988407fda64 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a7b1de1ca1e7f875ae483c27efb1ef4b7c3f45f2b27b040a534f5988407fda64->enter($__internal_a7b1de1ca1e7f875ae483c27efb1ef4b7c3f45f2b27b040a534f5988407fda64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_37ea627f7799d7d329229580c4ece9c68f38da3257d524ff0560ebac574ba1ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37ea627f7799d7d329229580c4ece9c68f38da3257d524ff0560ebac574ba1ab->enter($__internal_37ea627f7799d7d329229580c4ece9c68f38da3257d524ff0560ebac574ba1ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_a7b1de1ca1e7f875ae483c27efb1ef4b7c3f45f2b27b040a534f5988407fda64->leave($__internal_a7b1de1ca1e7f875ae483c27efb1ef4b7c3f45f2b27b040a534f5988407fda64_prof);

        
        $__internal_37ea627f7799d7d329229580c4ece9c68f38da3257d524ff0560ebac574ba1ab->leave($__internal_37ea627f7799d7d329229580c4ece9c68f38da3257d524ff0560ebac574ba1ab_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/chevron-right.svg");
    }
}
