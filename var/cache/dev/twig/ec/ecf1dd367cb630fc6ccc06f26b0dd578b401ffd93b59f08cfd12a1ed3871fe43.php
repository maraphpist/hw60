<?php

/* @Framework/Form/widget_attributes.html.php */
class __TwigTemplate_f3ed18cf424b56f95d3e2b09d1f7ff84c2cf2d9dcde67f8574d093c3bac364ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ba01f9748b50b23ba8b21daf05f0e299a9b0b411517842bde6cd61423b15369 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ba01f9748b50b23ba8b21daf05f0e299a9b0b411517842bde6cd61423b15369->enter($__internal_0ba01f9748b50b23ba8b21daf05f0e299a9b0b411517842bde6cd61423b15369_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        $__internal_436aa53b1bbedac31966a32c6f0531d0cf916ff2989bf6b7e40e2bb4ab822ddb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_436aa53b1bbedac31966a32c6f0531d0cf916ff2989bf6b7e40e2bb4ab822ddb->enter($__internal_436aa53b1bbedac31966a32c6f0531d0cf916ff2989bf6b7e40e2bb4ab822ddb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_0ba01f9748b50b23ba8b21daf05f0e299a9b0b411517842bde6cd61423b15369->leave($__internal_0ba01f9748b50b23ba8b21daf05f0e299a9b0b411517842bde6cd61423b15369_prof);

        
        $__internal_436aa53b1bbedac31966a32c6f0531d0cf916ff2989bf6b7e40e2bb4ab822ddb->leave($__internal_436aa53b1bbedac31966a32c6f0531d0cf916ff2989bf6b7e40e2bb4ab822ddb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_attributes.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widget_attributes.html.php");
    }
}
