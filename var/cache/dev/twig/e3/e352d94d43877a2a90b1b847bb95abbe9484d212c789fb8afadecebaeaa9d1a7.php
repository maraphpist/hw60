<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_9f4f286450227213bf58a0359df4d302b4fd04e289a400a5467910f0c3276bf1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3a3ac27e932a1bbbf82d49e7bd84ac9b0499f7856bf4318767f0ec3f1fca147d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a3ac27e932a1bbbf82d49e7bd84ac9b0499f7856bf4318767f0ec3f1fca147d->enter($__internal_3a3ac27e932a1bbbf82d49e7bd84ac9b0499f7856bf4318767f0ec3f1fca147d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $__internal_6a292d99b5fca13a4d4a89d19411d3c7e607a3988922cc58b0d75afb87e9925b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a292d99b5fca13a4d4a89d19411d3c7e607a3988922cc58b0d75afb87e9925b->enter($__internal_6a292d99b5fca13a4d4a89d19411d3c7e607a3988922cc58b0d75afb87e9925b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3a3ac27e932a1bbbf82d49e7bd84ac9b0499f7856bf4318767f0ec3f1fca147d->leave($__internal_3a3ac27e932a1bbbf82d49e7bd84ac9b0499f7856bf4318767f0ec3f1fca147d_prof);

        
        $__internal_6a292d99b5fca13a4d4a89d19411d3c7e607a3988922cc58b0d75afb87e9925b->leave($__internal_6a292d99b5fca13a4d4a89d19411d3c7e607a3988922cc58b0d75afb87e9925b_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_4d9b8013d30307a1e58fa982b7b855c154c75389c00212c2820aefdc51ff0450 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d9b8013d30307a1e58fa982b7b855c154c75389c00212c2820aefdc51ff0450->enter($__internal_4d9b8013d30307a1e58fa982b7b855c154c75389c00212c2820aefdc51ff0450_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_ce7ae77a2ec5776d74249166ae399b6564fd72271a2a95ed8e4cdae9947a21e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce7ae77a2ec5776d74249166ae399b6564fd72271a2a95ed8e4cdae9947a21e2->enter($__internal_ce7ae77a2ec5776d74249166ae399b6564fd72271a2a95ed8e4cdae9947a21e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_ce7ae77a2ec5776d74249166ae399b6564fd72271a2a95ed8e4cdae9947a21e2->leave($__internal_ce7ae77a2ec5776d74249166ae399b6564fd72271a2a95ed8e4cdae9947a21e2_prof);

        
        $__internal_4d9b8013d30307a1e58fa982b7b855c154c75389c00212c2820aefdc51ff0450->leave($__internal_4d9b8013d30307a1e58fa982b7b855c154c75389c00212c2820aefdc51ff0450_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:check_email.html.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/check_email.html.twig");
    }
}
