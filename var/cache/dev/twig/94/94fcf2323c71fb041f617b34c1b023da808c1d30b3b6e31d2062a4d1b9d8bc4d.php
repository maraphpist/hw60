<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_565d7b34017e7b0d00cabe1ad15d79198e7f69bc178b026aab416bff8ead0d57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_da623a334fc066bc20df7270402b9013529b1a0d3691ffc495ebf8f21fbb03b3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_da623a334fc066bc20df7270402b9013529b1a0d3691ffc495ebf8f21fbb03b3->enter($__internal_da623a334fc066bc20df7270402b9013529b1a0d3691ffc495ebf8f21fbb03b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_907cceaac7215d6cb0ed5b44fd868d9e7979a90810877c7580434793d22580a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_907cceaac7215d6cb0ed5b44fd868d9e7979a90810877c7580434793d22580a0->enter($__internal_907cceaac7215d6cb0ed5b44fd868d9e7979a90810877c7580434793d22580a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_da623a334fc066bc20df7270402b9013529b1a0d3691ffc495ebf8f21fbb03b3->leave($__internal_da623a334fc066bc20df7270402b9013529b1a0d3691ffc495ebf8f21fbb03b3_prof);

        
        $__internal_907cceaac7215d6cb0ed5b44fd868d9e7979a90810877c7580434793d22580a0->leave($__internal_907cceaac7215d6cb0ed5b44fd868d9e7979a90810877c7580434793d22580a0_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.rdf.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
