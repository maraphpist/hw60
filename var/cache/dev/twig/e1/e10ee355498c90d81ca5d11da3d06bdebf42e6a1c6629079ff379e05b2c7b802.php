<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_090b1f152a917b9f715044b30f08eed4dd30828b3a94637058ffbccb5cdae65b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9aa526a275db7e38042dfe6a8b67ac41b951e67f68a612d972e5ab214b82ca5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9aa526a275db7e38042dfe6a8b67ac41b951e67f68a612d972e5ab214b82ca5a->enter($__internal_9aa526a275db7e38042dfe6a8b67ac41b951e67f68a612d972e5ab214b82ca5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_b8191b92d5baa94d309b1d46601cd5b10691896faf119ef1767f5eae146c7525 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8191b92d5baa94d309b1d46601cd5b10691896faf119ef1767f5eae146c7525->enter($__internal_b8191b92d5baa94d309b1d46601cd5b10691896faf119ef1767f5eae146c7525_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_9aa526a275db7e38042dfe6a8b67ac41b951e67f68a612d972e5ab214b82ca5a->leave($__internal_9aa526a275db7e38042dfe6a8b67ac41b951e67f68a612d972e5ab214b82ca5a_prof);

        
        $__internal_b8191b92d5baa94d309b1d46601cd5b10691896faf119ef1767f5eae146c7525->leave($__internal_b8191b92d5baa94d309b1d46601cd5b10691896faf119ef1767f5eae146c7525_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
