<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_5c78ddec2dd0213e300968bd3cecc18c8277369c7402f3d95247e69c4acc98c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_43a1c3b272e3d9f6200414a9ec1b0b3a60128724e2707b65e3b6abc373b7c949 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_43a1c3b272e3d9f6200414a9ec1b0b3a60128724e2707b65e3b6abc373b7c949->enter($__internal_43a1c3b272e3d9f6200414a9ec1b0b3a60128724e2707b65e3b6abc373b7c949_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_363dcafef82d0a5dc0d3881cc32ae6117c2d262ad41b420bb2e95c4525871645 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_363dcafef82d0a5dc0d3881cc32ae6117c2d262ad41b420bb2e95c4525871645->enter($__internal_363dcafef82d0a5dc0d3881cc32ae6117c2d262ad41b420bb2e95c4525871645_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_43a1c3b272e3d9f6200414a9ec1b0b3a60128724e2707b65e3b6abc373b7c949->leave($__internal_43a1c3b272e3d9f6200414a9ec1b0b3a60128724e2707b65e3b6abc373b7c949_prof);

        
        $__internal_363dcafef82d0a5dc0d3881cc32ae6117c2d262ad41b420bb2e95c4525871645->leave($__internal_363dcafef82d0a5dc0d3881cc32ae6117c2d262ad41b420bb2e95c4525871645_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_0dd94416e17c6c083a721d1628fbcf6c3174b14a5b90ee335431fb4f63eea644 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0dd94416e17c6c083a721d1628fbcf6c3174b14a5b90ee335431fb4f63eea644->enter($__internal_0dd94416e17c6c083a721d1628fbcf6c3174b14a5b90ee335431fb4f63eea644_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_ff97d445e542d9abe0a4de36926bc84702ad6cb1ce630d83e51ecdebc48984e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff97d445e542d9abe0a4de36926bc84702ad6cb1ce630d83e51ecdebc48984e9->enter($__internal_ff97d445e542d9abe0a4de36926bc84702ad6cb1ce630d83e51ecdebc48984e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_ff97d445e542d9abe0a4de36926bc84702ad6cb1ce630d83e51ecdebc48984e9->leave($__internal_ff97d445e542d9abe0a4de36926bc84702ad6cb1ce630d83e51ecdebc48984e9_prof);

        
        $__internal_0dd94416e17c6c083a721d1628fbcf6c3174b14a5b90ee335431fb4f63eea644->leave($__internal_0dd94416e17c6c083a721d1628fbcf6c3174b14a5b90ee335431fb4f63eea644_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_e7f998b7a97ecef10cd7f05ab1c6b54c6c100545c1d44a82981958adcf630051 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e7f998b7a97ecef10cd7f05ab1c6b54c6c100545c1d44a82981958adcf630051->enter($__internal_e7f998b7a97ecef10cd7f05ab1c6b54c6c100545c1d44a82981958adcf630051_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_8bf829cddc04fc94f27326fac8e05541131c815ab2315db0a9939a9c59bf1481 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8bf829cddc04fc94f27326fac8e05541131c815ab2315db0a9939a9c59bf1481->enter($__internal_8bf829cddc04fc94f27326fac8e05541131c815ab2315db0a9939a9c59bf1481_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_8bf829cddc04fc94f27326fac8e05541131c815ab2315db0a9939a9c59bf1481->leave($__internal_8bf829cddc04fc94f27326fac8e05541131c815ab2315db0a9939a9c59bf1481_prof);

        
        $__internal_e7f998b7a97ecef10cd7f05ab1c6b54c6c100545c1d44a82981958adcf630051->leave($__internal_e7f998b7a97ecef10cd7f05ab1c6b54c6c100545c1d44a82981958adcf630051_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
