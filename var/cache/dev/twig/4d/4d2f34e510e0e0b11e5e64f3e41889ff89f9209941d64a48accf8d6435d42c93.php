<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_6df3dfb24239c54f6ce06832815f88ac08190800daead0570c872412557bb9f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1938e0e30052c4d2d66f7a9bd66bcd75de7d5e475966cd61e39a912fe08dedb7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1938e0e30052c4d2d66f7a9bd66bcd75de7d5e475966cd61e39a912fe08dedb7->enter($__internal_1938e0e30052c4d2d66f7a9bd66bcd75de7d5e475966cd61e39a912fe08dedb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_2d626cdc3f3fba52aa4ef38faaa6db93c024cc63989a3c93f7902fce1861c29d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d626cdc3f3fba52aa4ef38faaa6db93c024cc63989a3c93f7902fce1861c29d->enter($__internal_2d626cdc3f3fba52aa4ef38faaa6db93c024cc63989a3c93f7902fce1861c29d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_1938e0e30052c4d2d66f7a9bd66bcd75de7d5e475966cd61e39a912fe08dedb7->leave($__internal_1938e0e30052c4d2d66f7a9bd66bcd75de7d5e475966cd61e39a912fe08dedb7_prof);

        
        $__internal_2d626cdc3f3fba52aa4ef38faaa6db93c024cc63989a3c93f7902fce1861c29d->leave($__internal_2d626cdc3f3fba52aa4ef38faaa6db93c024cc63989a3c93f7902fce1861c29d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}
