<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_ba0aca08246e24f2528322e232d4de5528dafa300cad831a16f152ed5a73d85b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_23898b5925e263d86b62f13c0a3128fb06ca99f66fe16f8c100f2ce56f4e7933 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_23898b5925e263d86b62f13c0a3128fb06ca99f66fe16f8c100f2ce56f4e7933->enter($__internal_23898b5925e263d86b62f13c0a3128fb06ca99f66fe16f8c100f2ce56f4e7933_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $__internal_ce1092129b8ec863266ad3556a715381e7dc57315e95fcfcd404fa24dbabc15b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce1092129b8ec863266ad3556a715381e7dc57315e95fcfcd404fa24dbabc15b->enter($__internal_ce1092129b8ec863266ad3556a715381e7dc57315e95fcfcd404fa24dbabc15b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_23898b5925e263d86b62f13c0a3128fb06ca99f66fe16f8c100f2ce56f4e7933->leave($__internal_23898b5925e263d86b62f13c0a3128fb06ca99f66fe16f8c100f2ce56f4e7933_prof);

        
        $__internal_ce1092129b8ec863266ad3556a715381e7dc57315e95fcfcd404fa24dbabc15b->leave($__internal_ce1092129b8ec863266ad3556a715381e7dc57315e95fcfcd404fa24dbabc15b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_10e8d473add51ae4973498e64837d5146e3970a23531765ab73c92df878ba1d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_10e8d473add51ae4973498e64837d5146e3970a23531765ab73c92df878ba1d9->enter($__internal_10e8d473add51ae4973498e64837d5146e3970a23531765ab73c92df878ba1d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1360a6b31d99ca960cc8ef1598b76c4c0491018da88f27423356a5d91a8f2817 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1360a6b31d99ca960cc8ef1598b76c4c0491018da88f27423356a5d91a8f2817->enter($__internal_1360a6b31d99ca960cc8ef1598b76c4c0491018da88f27423356a5d91a8f2817_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_1360a6b31d99ca960cc8ef1598b76c4c0491018da88f27423356a5d91a8f2817->leave($__internal_1360a6b31d99ca960cc8ef1598b76c4c0491018da88f27423356a5d91a8f2817_prof);

        
        $__internal_10e8d473add51ae4973498e64837d5146e3970a23531765ab73c92df878ba1d9->leave($__internal_10e8d473add51ae4973498e64837d5146e3970a23531765ab73c92df878ba1d9_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:reset.html.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}
