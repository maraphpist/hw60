<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_fb7333f16a6803cc6226c73ee2f5f0e9486aaa095366b8f853b49a2b908671e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_11b0cb39575e9d06e9768b44c1df2f46997eb4d715d3432105991418fd238de5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_11b0cb39575e9d06e9768b44c1df2f46997eb4d715d3432105991418fd238de5->enter($__internal_11b0cb39575e9d06e9768b44c1df2f46997eb4d715d3432105991418fd238de5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $__internal_56f7408dcccfb977abefc10698c1d7f9b2de6821644da3c7b315ca1a156e1ab6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56f7408dcccfb977abefc10698c1d7f9b2de6821644da3c7b315ca1a156e1ab6->enter($__internal_56f7408dcccfb977abefc10698c1d7f9b2de6821644da3c7b315ca1a156e1ab6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_11b0cb39575e9d06e9768b44c1df2f46997eb4d715d3432105991418fd238de5->leave($__internal_11b0cb39575e9d06e9768b44c1df2f46997eb4d715d3432105991418fd238de5_prof);

        
        $__internal_56f7408dcccfb977abefc10698c1d7f9b2de6821644da3c7b315ca1a156e1ab6->leave($__internal_56f7408dcccfb977abefc10698c1d7f9b2de6821644da3c7b315ca1a156e1ab6_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_78e6a1e026e4f135e0f40fbf3231d96147d21c46040fd125656a696452ca2e3e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_78e6a1e026e4f135e0f40fbf3231d96147d21c46040fd125656a696452ca2e3e->enter($__internal_78e6a1e026e4f135e0f40fbf3231d96147d21c46040fd125656a696452ca2e3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1b86c8fadbdb3b61700fb97a1c57fe7ffa08545e41d356b1af8c0771940b663a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b86c8fadbdb3b61700fb97a1c57fe7ffa08545e41d356b1af8c0771940b663a->enter($__internal_1b86c8fadbdb3b61700fb97a1c57fe7ffa08545e41d356b1af8c0771940b663a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_1b86c8fadbdb3b61700fb97a1c57fe7ffa08545e41d356b1af8c0771940b663a->leave($__internal_1b86c8fadbdb3b61700fb97a1c57fe7ffa08545e41d356b1af8c0771940b663a_prof);

        
        $__internal_78e6a1e026e4f135e0f40fbf3231d96147d21c46040fd125656a696452ca2e3e->leave($__internal_78e6a1e026e4f135e0f40fbf3231d96147d21c46040fd125656a696452ca2e3e_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:show.html.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show.html.twig");
    }
}
