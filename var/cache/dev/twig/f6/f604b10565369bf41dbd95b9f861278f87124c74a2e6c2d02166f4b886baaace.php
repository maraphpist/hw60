<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_94f2eb4198013dc295c045882ffcee0c845e89a9371ac29f4ad50235d639f4ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0cc570ad407272ed016231c94aa218f8c9984da0106759d7e7c544e8189f4914 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0cc570ad407272ed016231c94aa218f8c9984da0106759d7e7c544e8189f4914->enter($__internal_0cc570ad407272ed016231c94aa218f8c9984da0106759d7e7c544e8189f4914_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_01f6bdaba062fb78215d33cb7518896351c131e905b34f467cddd1a4fa6555ea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_01f6bdaba062fb78215d33cb7518896351c131e905b34f467cddd1a4fa6555ea->enter($__internal_01f6bdaba062fb78215d33cb7518896351c131e905b34f467cddd1a4fa6555ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_0cc570ad407272ed016231c94aa218f8c9984da0106759d7e7c544e8189f4914->leave($__internal_0cc570ad407272ed016231c94aa218f8c9984da0106759d7e7c544e8189f4914_prof);

        
        $__internal_01f6bdaba062fb78215d33cb7518896351c131e905b34f467cddd1a4fa6555ea->leave($__internal_01f6bdaba062fb78215d33cb7518896351c131e905b34f467cddd1a4fa6555ea_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}
