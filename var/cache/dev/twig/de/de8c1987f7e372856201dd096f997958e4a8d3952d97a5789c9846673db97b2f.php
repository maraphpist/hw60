<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_010742bacd7922f8c0ef3868b50c1fd85132c12a1b910d45f0522f422e756bf5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_49b56f08e8a5a0034467aaaef7d41769a061e4aad176e04def1b593aaef9407e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_49b56f08e8a5a0034467aaaef7d41769a061e4aad176e04def1b593aaef9407e->enter($__internal_49b56f08e8a5a0034467aaaef7d41769a061e4aad176e04def1b593aaef9407e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $__internal_457424c1f77f7b99a4a814001cd98b8fab5f4651b22c216b1823669f13cd083d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_457424c1f77f7b99a4a814001cd98b8fab5f4651b22c216b1823669f13cd083d->enter($__internal_457424c1f77f7b99a4a814001cd98b8fab5f4651b22c216b1823669f13cd083d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_49b56f08e8a5a0034467aaaef7d41769a061e4aad176e04def1b593aaef9407e->leave($__internal_49b56f08e8a5a0034467aaaef7d41769a061e4aad176e04def1b593aaef9407e_prof);

        
        $__internal_457424c1f77f7b99a4a814001cd98b8fab5f4651b22c216b1823669f13cd083d->leave($__internal_457424c1f77f7b99a4a814001cd98b8fab5f4651b22c216b1823669f13cd083d_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_7c36a98a41b992f527e287f29b67b0e72b5db419eb80d433597919a568cb33f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c36a98a41b992f527e287f29b67b0e72b5db419eb80d433597919a568cb33f9->enter($__internal_7c36a98a41b992f527e287f29b67b0e72b5db419eb80d433597919a568cb33f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_cb946f287af7aa457e672467fb33ae48d73f1918288519fc0f3c4e1819e0c8b6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb946f287af7aa457e672467fb33ae48d73f1918288519fc0f3c4e1819e0c8b6->enter($__internal_cb946f287af7aa457e672467fb33ae48d73f1918288519fc0f3c4e1819e0c8b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_cb946f287af7aa457e672467fb33ae48d73f1918288519fc0f3c4e1819e0c8b6->leave($__internal_cb946f287af7aa457e672467fb33ae48d73f1918288519fc0f3c4e1819e0c8b6_prof);

        
        $__internal_7c36a98a41b992f527e287f29b67b0e72b5db419eb80d433597919a568cb33f9->leave($__internal_7c36a98a41b992f527e287f29b67b0e72b5db419eb80d433597919a568cb33f9_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_21c1e413ef67aa43e7b4a457d2ac5f5e4e35475f0da3f5db77ef7700c13c8695 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21c1e413ef67aa43e7b4a457d2ac5f5e4e35475f0da3f5db77ef7700c13c8695->enter($__internal_21c1e413ef67aa43e7b4a457d2ac5f5e4e35475f0da3f5db77ef7700c13c8695_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_e0810f0aef8740969cefd349f174d5290037003244d1c739ba017ac2ffd4b8ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0810f0aef8740969cefd349f174d5290037003244d1c739ba017ac2ffd4b8ab->enter($__internal_e0810f0aef8740969cefd349f174d5290037003244d1c739ba017ac2ffd4b8ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_e0810f0aef8740969cefd349f174d5290037003244d1c739ba017ac2ffd4b8ab->leave($__internal_e0810f0aef8740969cefd349f174d5290037003244d1c739ba017ac2ffd4b8ab_prof);

        
        $__internal_21c1e413ef67aa43e7b4a457d2ac5f5e4e35475f0da3f5db77ef7700c13c8695->leave($__internal_21c1e413ef67aa43e7b4a457d2ac5f5e4e35475f0da3f5db77ef7700c13c8695_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_f6ad4b371ae0263653705913bc16d9982531a5c6550733f096e5004b08e89a99 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f6ad4b371ae0263653705913bc16d9982531a5c6550733f096e5004b08e89a99->enter($__internal_f6ad4b371ae0263653705913bc16d9982531a5c6550733f096e5004b08e89a99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_f33090d9b59c8077ab5d81dd6f2750ff66d00f644a3f4d6bdb2103c3050143de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f33090d9b59c8077ab5d81dd6f2750ff66d00f644a3f4d6bdb2103c3050143de->enter($__internal_f33090d9b59c8077ab5d81dd6f2750ff66d00f644a3f4d6bdb2103c3050143de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_f33090d9b59c8077ab5d81dd6f2750ff66d00f644a3f4d6bdb2103c3050143de->leave($__internal_f33090d9b59c8077ab5d81dd6f2750ff66d00f644a3f4d6bdb2103c3050143de_prof);

        
        $__internal_f6ad4b371ae0263653705913bc16d9982531a5c6550733f096e5004b08e89a99->leave($__internal_f6ad4b371ae0263653705913bc16d9982531a5c6550733f096e5004b08e89a99_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
