<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_27fcc32cbc727b456cb6bdc4703e93e2968b841d299cdcc212c3aff97df9e93e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ba23cedff8171e4e7b609a75586d4636fbb63d70d598dff7c72016b38ff533d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba23cedff8171e4e7b609a75586d4636fbb63d70d598dff7c72016b38ff533d2->enter($__internal_ba23cedff8171e4e7b609a75586d4636fbb63d70d598dff7c72016b38ff533d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_4a2fc808680d6a682eb01ef071b8037f6330cf98886360e40240435bf64470e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a2fc808680d6a682eb01ef071b8037f6330cf98886360e40240435bf64470e1->enter($__internal_4a2fc808680d6a682eb01ef071b8037f6330cf98886360e40240435bf64470e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_ba23cedff8171e4e7b609a75586d4636fbb63d70d598dff7c72016b38ff533d2->leave($__internal_ba23cedff8171e4e7b609a75586d4636fbb63d70d598dff7c72016b38ff533d2_prof);

        
        $__internal_4a2fc808680d6a682eb01ef071b8037f6330cf98886360e40240435bf64470e1->leave($__internal_4a2fc808680d6a682eb01ef071b8037f6330cf98886360e40240435bf64470e1_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_285b4879a7c7d67e6423f1794307bb1fb7350f4f0a77b6d3f35e72f00dc0c279 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_285b4879a7c7d67e6423f1794307bb1fb7350f4f0a77b6d3f35e72f00dc0c279->enter($__internal_285b4879a7c7d67e6423f1794307bb1fb7350f4f0a77b6d3f35e72f00dc0c279_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_72f5e4d8e8d79eb316e91c1a68e6a0ebc01fda0aa80579d376b27112fa9c908a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_72f5e4d8e8d79eb316e91c1a68e6a0ebc01fda0aa80579d376b27112fa9c908a->enter($__internal_72f5e4d8e8d79eb316e91c1a68e6a0ebc01fda0aa80579d376b27112fa9c908a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_72f5e4d8e8d79eb316e91c1a68e6a0ebc01fda0aa80579d376b27112fa9c908a->leave($__internal_72f5e4d8e8d79eb316e91c1a68e6a0ebc01fda0aa80579d376b27112fa9c908a_prof);

        
        $__internal_285b4879a7c7d67e6423f1794307bb1fb7350f4f0a77b6d3f35e72f00dc0c279->leave($__internal_285b4879a7c7d67e6423f1794307bb1fb7350f4f0a77b6d3f35e72f00dc0c279_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
