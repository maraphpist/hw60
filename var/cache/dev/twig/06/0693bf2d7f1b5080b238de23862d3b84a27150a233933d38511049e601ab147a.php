<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_bcee72b5a49fa11ebb864b9c54977b53673b38d5ff421148bb4188ba0eead1f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b792bdd63886760ec871ec947372d193977624cd87c8d7c1df9ca57518cebef9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b792bdd63886760ec871ec947372d193977624cd87c8d7c1df9ca57518cebef9->enter($__internal_b792bdd63886760ec871ec947372d193977624cd87c8d7c1df9ca57518cebef9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_71becd9f86bc5d4395c6686ed4b716b8b3bcbe0d760d3eb7c33890e3a34675e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_71becd9f86bc5d4395c6686ed4b716b8b3bcbe0d760d3eb7c33890e3a34675e1->enter($__internal_71becd9f86bc5d4395c6686ed4b716b8b3bcbe0d760d3eb7c33890e3a34675e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_b792bdd63886760ec871ec947372d193977624cd87c8d7c1df9ca57518cebef9->leave($__internal_b792bdd63886760ec871ec947372d193977624cd87c8d7c1df9ca57518cebef9_prof);

        
        $__internal_71becd9f86bc5d4395c6686ed4b716b8b3bcbe0d760d3eb7c33890e3a34675e1->leave($__internal_71becd9f86bc5d4395c6686ed4b716b8b3bcbe0d760d3eb7c33890e3a34675e1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}
