<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_cf5a95adbac54adef95483068906b27542e973e054111880720610620c75663d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_07c4418e91fbec73ba2a81fee71b5a180eb27d132cc51b2c148ea6732c628966 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_07c4418e91fbec73ba2a81fee71b5a180eb27d132cc51b2c148ea6732c628966->enter($__internal_07c4418e91fbec73ba2a81fee71b5a180eb27d132cc51b2c148ea6732c628966_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $__internal_c5114595f0ba43f81bcf88db3d85440be42cd63de7c8e51f442d1a2ac7f986b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c5114595f0ba43f81bcf88db3d85440be42cd63de7c8e51f442d1a2ac7f986b1->enter($__internal_c5114595f0ba43f81bcf88db3d85440be42cd63de7c8e51f442d1a2ac7f986b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_07c4418e91fbec73ba2a81fee71b5a180eb27d132cc51b2c148ea6732c628966->leave($__internal_07c4418e91fbec73ba2a81fee71b5a180eb27d132cc51b2c148ea6732c628966_prof);

        
        $__internal_c5114595f0ba43f81bcf88db3d85440be42cd63de7c8e51f442d1a2ac7f986b1->leave($__internal_c5114595f0ba43f81bcf88db3d85440be42cd63de7c8e51f442d1a2ac7f986b1_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_0efaab7420a8b6470058a66c33c3cf57a48a37c2d1b746340f037e97ca524447 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0efaab7420a8b6470058a66c33c3cf57a48a37c2d1b746340f037e97ca524447->enter($__internal_0efaab7420a8b6470058a66c33c3cf57a48a37c2d1b746340f037e97ca524447_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_4685fa57d94dec7b6a4ad5dbcd6211f786825d0e2ab334945f61462c537c9e9d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4685fa57d94dec7b6a4ad5dbcd6211f786825d0e2ab334945f61462c537c9e9d->enter($__internal_4685fa57d94dec7b6a4ad5dbcd6211f786825d0e2ab334945f61462c537c9e9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_4685fa57d94dec7b6a4ad5dbcd6211f786825d0e2ab334945f61462c537c9e9d->leave($__internal_4685fa57d94dec7b6a4ad5dbcd6211f786825d0e2ab334945f61462c537c9e9d_prof);

        
        $__internal_0efaab7420a8b6470058a66c33c3cf57a48a37c2d1b746340f037e97ca524447->leave($__internal_0efaab7420a8b6470058a66c33c3cf57a48a37c2d1b746340f037e97ca524447_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:list.html.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list.html.twig");
    }
}
