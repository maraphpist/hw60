<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_6411630e49d7556c6b402a950d82b722ad3ab8c454cda3acc41d70aeb16697a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_41eaf5a1da01ebaf1e6ab9e92662e9bf08a42aab1bc5f585caa7c272a77f51be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_41eaf5a1da01ebaf1e6ab9e92662e9bf08a42aab1bc5f585caa7c272a77f51be->enter($__internal_41eaf5a1da01ebaf1e6ab9e92662e9bf08a42aab1bc5f585caa7c272a77f51be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_7a01981abcf9a190e82eff6709766d365c347cad377cc740520f4dc6c5537c1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a01981abcf9a190e82eff6709766d365c347cad377cc740520f4dc6c5537c1c->enter($__internal_7a01981abcf9a190e82eff6709766d365c347cad377cc740520f4dc6c5537c1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_41eaf5a1da01ebaf1e6ab9e92662e9bf08a42aab1bc5f585caa7c272a77f51be->leave($__internal_41eaf5a1da01ebaf1e6ab9e92662e9bf08a42aab1bc5f585caa7c272a77f51be_prof);

        
        $__internal_7a01981abcf9a190e82eff6709766d365c347cad377cc740520f4dc6c5537c1c->leave($__internal_7a01981abcf9a190e82eff6709766d365c347cad377cc740520f4dc6c5537c1c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}
