<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_acacba1794e10f6c9f681ada3edf5afef455597ac8d2cf1b0f35f2e08cb37226 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_32fa431d794adeb40089e508429a0b3f6d71a3e439b8028f779ab48460a98306 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_32fa431d794adeb40089e508429a0b3f6d71a3e439b8028f779ab48460a98306->enter($__internal_32fa431d794adeb40089e508429a0b3f6d71a3e439b8028f779ab48460a98306_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        $__internal_d86323e2c9c45ca42b5223b5a62fe714b48333bdaaf5cbbd0aa564be24a9299c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d86323e2c9c45ca42b5223b5a62fe714b48333bdaaf5cbbd0aa564be24a9299c->enter($__internal_d86323e2c9c45ca42b5223b5a62fe714b48333bdaaf5cbbd0aa564be24a9299c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_32fa431d794adeb40089e508429a0b3f6d71a3e439b8028f779ab48460a98306->leave($__internal_32fa431d794adeb40089e508429a0b3f6d71a3e439b8028f779ab48460a98306_prof);

        
        $__internal_d86323e2c9c45ca42b5223b5a62fe714b48333bdaaf5cbbd0aa564be24a9299c->leave($__internal_d86323e2c9c45ca42b5223b5a62fe714b48333bdaaf5cbbd0aa564be24a9299c_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.rdf.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
