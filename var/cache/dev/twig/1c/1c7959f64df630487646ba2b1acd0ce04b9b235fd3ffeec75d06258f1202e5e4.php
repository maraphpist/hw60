<?php

/* WebProfilerBundle:Collector:exception.css.twig */
class __TwigTemplate_f46d38e8029f4425ba8c0f8b593a179b49677011379ac2ad013818cd1992a1fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7bc10c7e5c31356d8a9af9faba8e0ec066eea5d24648fe73cd7da240f5846a8d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7bc10c7e5c31356d8a9af9faba8e0ec066eea5d24648fe73cd7da240f5846a8d->enter($__internal_7bc10c7e5c31356d8a9af9faba8e0ec066eea5d24648fe73cd7da240f5846a8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        $__internal_12968abd31e94b84dcb5186831817facbe24e2ece2bbc83a24b85e32d470daf6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12968abd31e94b84dcb5186831817facbe24e2ece2bbc83a24b85e32d470daf6->enter($__internal_12968abd31e94b84dcb5186831817facbe24e2ece2bbc83a24b85e32d470daf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper {
    min-height: auto;
}
";
        
        $__internal_7bc10c7e5c31356d8a9af9faba8e0ec066eea5d24648fe73cd7da240f5846a8d->leave($__internal_7bc10c7e5c31356d8a9af9faba8e0ec066eea5d24648fe73cd7da240f5846a8d_prof);

        
        $__internal_12968abd31e94b84dcb5186831817facbe24e2ece2bbc83a24b85e32d470daf6->leave($__internal_12968abd31e94b84dcb5186831817facbe24e2ece2bbc83a24b85e32d470daf6_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/exception.css.twig') }}

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper {
    min-height: auto;
}
", "WebProfilerBundle:Collector:exception.css.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.css.twig");
    }
}
