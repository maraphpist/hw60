<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_126f9d29f853e2cae2b1b3eae1a15db7b503b27c39443e9259a0801a9aea4fb6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bc9e3a808c45026139504719f701fb848bf0dc7eecb78e33f0a570d14bf280ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc9e3a808c45026139504719f701fb848bf0dc7eecb78e33f0a570d14bf280ae->enter($__internal_bc9e3a808c45026139504719f701fb848bf0dc7eecb78e33f0a570d14bf280ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $__internal_da52f6cdbaef9d9ab21fa44c8c11bc2d1b9ffa5486294b86a32130ff9336d153 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da52f6cdbaef9d9ab21fa44c8c11bc2d1b9ffa5486294b86a32130ff9336d153->enter($__internal_da52f6cdbaef9d9ab21fa44c8c11bc2d1b9ffa5486294b86a32130ff9336d153_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bc9e3a808c45026139504719f701fb848bf0dc7eecb78e33f0a570d14bf280ae->leave($__internal_bc9e3a808c45026139504719f701fb848bf0dc7eecb78e33f0a570d14bf280ae_prof);

        
        $__internal_da52f6cdbaef9d9ab21fa44c8c11bc2d1b9ffa5486294b86a32130ff9336d153->leave($__internal_da52f6cdbaef9d9ab21fa44c8c11bc2d1b9ffa5486294b86a32130ff9336d153_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_039ea265ff7844bf91292131de88f969ab26ffc8e735d3ec82671e8d537bf98d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_039ea265ff7844bf91292131de88f969ab26ffc8e735d3ec82671e8d537bf98d->enter($__internal_039ea265ff7844bf91292131de88f969ab26ffc8e735d3ec82671e8d537bf98d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_bd07e8f0c19d10a62b162797e56a6cdddd525cafc66502ea36f460b45a82726b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd07e8f0c19d10a62b162797e56a6cdddd525cafc66502ea36f460b45a82726b->enter($__internal_bd07e8f0c19d10a62b162797e56a6cdddd525cafc66502ea36f460b45a82726b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_bd07e8f0c19d10a62b162797e56a6cdddd525cafc66502ea36f460b45a82726b->leave($__internal_bd07e8f0c19d10a62b162797e56a6cdddd525cafc66502ea36f460b45a82726b_prof);

        
        $__internal_039ea265ff7844bf91292131de88f969ab26ffc8e735d3ec82671e8d537bf98d->leave($__internal_039ea265ff7844bf91292131de88f969ab26ffc8e735d3ec82671e8d537bf98d_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_4f9b646499f6673519b1e0681dc02d7618d801b9e6b986fb7e891fa40df824d7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f9b646499f6673519b1e0681dc02d7618d801b9e6b986fb7e891fa40df824d7->enter($__internal_4f9b646499f6673519b1e0681dc02d7618d801b9e6b986fb7e891fa40df824d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_40feb0c65da418bf1287a68b63598c1cab4667e755c394e7e5af5f8243cc3207 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_40feb0c65da418bf1287a68b63598c1cab4667e755c394e7e5af5f8243cc3207->enter($__internal_40feb0c65da418bf1287a68b63598c1cab4667e755c394e7e5af5f8243cc3207_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_40feb0c65da418bf1287a68b63598c1cab4667e755c394e7e5af5f8243cc3207->leave($__internal_40feb0c65da418bf1287a68b63598c1cab4667e755c394e7e5af5f8243cc3207_prof);

        
        $__internal_4f9b646499f6673519b1e0681dc02d7618d801b9e6b986fb7e891fa40df824d7->leave($__internal_4f9b646499f6673519b1e0681dc02d7618d801b9e6b986fb7e891fa40df824d7_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_2e5ae679eaae2871af22c7569e673e77162ee47510f5a2674709c5287ed9fce7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e5ae679eaae2871af22c7569e673e77162ee47510f5a2674709c5287ed9fce7->enter($__internal_2e5ae679eaae2871af22c7569e673e77162ee47510f5a2674709c5287ed9fce7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_0d9d883cad34d3906263eef4f3a1b3ff5884f444e734726854824ee0677412af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0d9d883cad34d3906263eef4f3a1b3ff5884f444e734726854824ee0677412af->enter($__internal_0d9d883cad34d3906263eef4f3a1b3ff5884f444e734726854824ee0677412af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 141)->display($context);
        
        $__internal_0d9d883cad34d3906263eef4f3a1b3ff5884f444e734726854824ee0677412af->leave($__internal_0d9d883cad34d3906263eef4f3a1b3ff5884f444e734726854824ee0677412af_prof);

        
        $__internal_2e5ae679eaae2871af22c7569e673e77162ee47510f5a2674709c5287ed9fce7->leave($__internal_2e5ae679eaae2871af22c7569e673e77162ee47510f5a2674709c5287ed9fce7_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
