<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_d4bbefb3b0dd532cc5b5f0a8b92e321fb27daac20e573b0a169576677f285fe4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_97bea5568fd6ca190c05d14419d29cfaa00f1a1a045e1a7d403027d09f3d3350 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_97bea5568fd6ca190c05d14419d29cfaa00f1a1a045e1a7d403027d09f3d3350->enter($__internal_97bea5568fd6ca190c05d14419d29cfaa00f1a1a045e1a7d403027d09f3d3350_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_3f587065d3319dea7d8767019c2eb4cb1d2b85f19be84cd554b99319a5a5d44a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f587065d3319dea7d8767019c2eb4cb1d2b85f19be84cd554b99319a5a5d44a->enter($__internal_3f587065d3319dea7d8767019c2eb4cb1d2b85f19be84cd554b99319a5a5d44a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_97bea5568fd6ca190c05d14419d29cfaa00f1a1a045e1a7d403027d09f3d3350->leave($__internal_97bea5568fd6ca190c05d14419d29cfaa00f1a1a045e1a7d403027d09f3d3350_prof);

        
        $__internal_3f587065d3319dea7d8767019c2eb4cb1d2b85f19be84cd554b99319a5a5d44a->leave($__internal_3f587065d3319dea7d8767019c2eb4cb1d2b85f19be84cd554b99319a5a5d44a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
