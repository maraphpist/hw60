<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_10f4786686d89a97183133610a4e6e9b6adc51207bfa05e3c6aa1b1d738a1018 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a8cd9599a527b4e67913b0aaaf2178b046da2cfb5fd043646bf60f35c9d77e31 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8cd9599a527b4e67913b0aaaf2178b046da2cfb5fd043646bf60f35c9d77e31->enter($__internal_a8cd9599a527b4e67913b0aaaf2178b046da2cfb5fd043646bf60f35c9d77e31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_d9a67cd4fe6d1d55b33fed34a1b4a2e1e83b09b556ee591a7f8871fa1cf52364 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d9a67cd4fe6d1d55b33fed34a1b4a2e1e83b09b556ee591a7f8871fa1cf52364->enter($__internal_d9a67cd4fe6d1d55b33fed34a1b4a2e1e83b09b556ee591a7f8871fa1cf52364_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_a8cd9599a527b4e67913b0aaaf2178b046da2cfb5fd043646bf60f35c9d77e31->leave($__internal_a8cd9599a527b4e67913b0aaaf2178b046da2cfb5fd043646bf60f35c9d77e31_prof);

        
        $__internal_d9a67cd4fe6d1d55b33fed34a1b4a2e1e83b09b556ee591a7f8871fa1cf52364->leave($__internal_d9a67cd4fe6d1d55b33fed34a1b4a2e1e83b09b556ee591a7f8871fa1cf52364_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}
