<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_88aa5590e1728b9aa9f696c69b5f470835caa9d563c4be92d673ae82c175976d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_01310e78dee7a4f0f05d6c8acad5697bca709c4498b99778fed8cef87f897661 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01310e78dee7a4f0f05d6c8acad5697bca709c4498b99778fed8cef87f897661->enter($__internal_01310e78dee7a4f0f05d6c8acad5697bca709c4498b99778fed8cef87f897661_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        $__internal_aa3fd496b47c62ee7fc2aced1758af9a37ba796d62d40794a331280e852e2a92 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa3fd496b47c62ee7fc2aced1758af9a37ba796d62d40794a331280e852e2a92->enter($__internal_aa3fd496b47c62ee7fc2aced1758af9a37ba796d62d40794a331280e852e2a92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_01310e78dee7a4f0f05d6c8acad5697bca709c4498b99778fed8cef87f897661->leave($__internal_01310e78dee7a4f0f05d6c8acad5697bca709c4498b99778fed8cef87f897661_prof);

        
        $__internal_aa3fd496b47c62ee7fc2aced1758af9a37ba796d62d40794a331280e852e2a92->leave($__internal_aa3fd496b47c62ee7fc2aced1758af9a37ba796d62d40794a331280e852e2a92_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/repeated_row.html.php");
    }
}
