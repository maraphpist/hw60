<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_624075785c095260ceab89457b4dac6a48a241898921aeb6cd8257a204ee9b36 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_970b3dd31de37047854fb6b65569fb8d67dc4c0571f59322ea9fe4704f192f02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_970b3dd31de37047854fb6b65569fb8d67dc4c0571f59322ea9fe4704f192f02->enter($__internal_970b3dd31de37047854fb6b65569fb8d67dc4c0571f59322ea9fe4704f192f02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        $__internal_f25a4fe177dd6d70b2726aa78a2917776c04c356697c7ca0aedceff560974fca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f25a4fe177dd6d70b2726aa78a2917776c04c356697c7ca0aedceff560974fca->enter($__internal_f25a4fe177dd6d70b2726aa78a2917776c04c356697c7ca0aedceff560974fca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_970b3dd31de37047854fb6b65569fb8d67dc4c0571f59322ea9fe4704f192f02->leave($__internal_970b3dd31de37047854fb6b65569fb8d67dc4c0571f59322ea9fe4704f192f02_prof);

        
        $__internal_f25a4fe177dd6d70b2726aa78a2917776c04c356697c7ca0aedceff560974fca->leave($__internal_f25a4fe177dd6d70b2726aa78a2917776c04c356697c7ca0aedceff560974fca_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/form_widget_compound.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_compound.html.php");
    }
}
