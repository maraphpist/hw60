<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_d9e1981aa8e284b05e9caf5b759e27b33c515389824ce30e73ce4ddfcc60ec9c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_523c76a79df8c327176a72e01b0ef0a2a0725b7768e609078f1e176b4a0fab63 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_523c76a79df8c327176a72e01b0ef0a2a0725b7768e609078f1e176b4a0fab63->enter($__internal_523c76a79df8c327176a72e01b0ef0a2a0725b7768e609078f1e176b4a0fab63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $__internal_a6378638fec97a299f8dc18434eba5da52e79d66bb3a87134821107931b7d93b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6378638fec97a299f8dc18434eba5da52e79d66bb3a87134821107931b7d93b->enter($__internal_a6378638fec97a299f8dc18434eba5da52e79d66bb3a87134821107931b7d93b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_523c76a79df8c327176a72e01b0ef0a2a0725b7768e609078f1e176b4a0fab63->leave($__internal_523c76a79df8c327176a72e01b0ef0a2a0725b7768e609078f1e176b4a0fab63_prof);

        
        $__internal_a6378638fec97a299f8dc18434eba5da52e79d66bb3a87134821107931b7d93b->leave($__internal_a6378638fec97a299f8dc18434eba5da52e79d66bb3a87134821107931b7d93b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_bef697c87f6e9e1e1f3ac2a732ea5ca43b1d6c26ea81381ec9da8f0fa1bb73ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bef697c87f6e9e1e1f3ac2a732ea5ca43b1d6c26ea81381ec9da8f0fa1bb73ce->enter($__internal_bef697c87f6e9e1e1f3ac2a732ea5ca43b1d6c26ea81381ec9da8f0fa1bb73ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1e5500abe387663cfa857e685d3b0b8046e22ad1b8dde4769f850c8b27bd09da = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e5500abe387663cfa857e685d3b0b8046e22ad1b8dde4769f850c8b27bd09da->enter($__internal_1e5500abe387663cfa857e685d3b0b8046e22ad1b8dde4769f850c8b27bd09da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_1e5500abe387663cfa857e685d3b0b8046e22ad1b8dde4769f850c8b27bd09da->leave($__internal_1e5500abe387663cfa857e685d3b0b8046e22ad1b8dde4769f850c8b27bd09da_prof);

        
        $__internal_bef697c87f6e9e1e1f3ac2a732ea5ca43b1d6c26ea81381ec9da8f0fa1bb73ce->leave($__internal_bef697c87f6e9e1e1f3ac2a732ea5ca43b1d6c26ea81381ec9da8f0fa1bb73ce_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:edit.html.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/Group/edit.html.twig");
    }
}
