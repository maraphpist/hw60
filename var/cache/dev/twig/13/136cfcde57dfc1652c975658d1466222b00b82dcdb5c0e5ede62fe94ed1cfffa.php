<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_8bb5a475bcef4e0db251738308df8e47dbc48a1cd1c7120cdf9a8487afd6b071 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_35ffafaec3775ba884abc0ce06402e92b5b1a69ca28cdedc3e63ce262f95daab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35ffafaec3775ba884abc0ce06402e92b5b1a69ca28cdedc3e63ce262f95daab->enter($__internal_35ffafaec3775ba884abc0ce06402e92b5b1a69ca28cdedc3e63ce262f95daab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_6f7b17c02d74aa70651052006baeb5ee6f2b31ff0932e893d5306a6a84ab0efb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f7b17c02d74aa70651052006baeb5ee6f2b31ff0932e893d5306a6a84ab0efb->enter($__internal_6f7b17c02d74aa70651052006baeb5ee6f2b31ff0932e893d5306a6a84ab0efb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_35ffafaec3775ba884abc0ce06402e92b5b1a69ca28cdedc3e63ce262f95daab->leave($__internal_35ffafaec3775ba884abc0ce06402e92b5b1a69ca28cdedc3e63ce262f95daab_prof);

        
        $__internal_6f7b17c02d74aa70651052006baeb5ee6f2b31ff0932e893d5306a6a84ab0efb->leave($__internal_6f7b17c02d74aa70651052006baeb5ee6f2b31ff0932e893d5306a6a84ab0efb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
