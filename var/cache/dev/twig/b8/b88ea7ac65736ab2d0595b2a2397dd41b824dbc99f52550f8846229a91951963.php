<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_69e6d9723c4d7a5da85fb4917391785023e8450ec8c33c1d8eabb645a9fcee6e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_853502b5a27434449f82ee6bf8849ef6154a45fd5b337f70549863d66b5b9e50 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_853502b5a27434449f82ee6bf8849ef6154a45fd5b337f70549863d66b5b9e50->enter($__internal_853502b5a27434449f82ee6bf8849ef6154a45fd5b337f70549863d66b5b9e50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $__internal_31d81bad9fe106b24a7dc8c21c0ea88f0683b402d5c6a1cc3a63438118d3f193 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_31d81bad9fe106b24a7dc8c21c0ea88f0683b402d5c6a1cc3a63438118d3f193->enter($__internal_31d81bad9fe106b24a7dc8c21c0ea88f0683b402d5c6a1cc3a63438118d3f193_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_853502b5a27434449f82ee6bf8849ef6154a45fd5b337f70549863d66b5b9e50->leave($__internal_853502b5a27434449f82ee6bf8849ef6154a45fd5b337f70549863d66b5b9e50_prof);

        
        $__internal_31d81bad9fe106b24a7dc8c21c0ea88f0683b402d5c6a1cc3a63438118d3f193->leave($__internal_31d81bad9fe106b24a7dc8c21c0ea88f0683b402d5c6a1cc3a63438118d3f193_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_538ed111b0f3f0e42cdbe3cad1242850832615adc154c55a85632dbd8419ebaa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_538ed111b0f3f0e42cdbe3cad1242850832615adc154c55a85632dbd8419ebaa->enter($__internal_538ed111b0f3f0e42cdbe3cad1242850832615adc154c55a85632dbd8419ebaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_4ef672fe8a7c01ad3967692f42744bc5e3016b60611eee12ac6e1df08276586d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ef672fe8a7c01ad3967692f42744bc5e3016b60611eee12ac6e1df08276586d->enter($__internal_4ef672fe8a7c01ad3967692f42744bc5e3016b60611eee12ac6e1df08276586d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_4ef672fe8a7c01ad3967692f42744bc5e3016b60611eee12ac6e1df08276586d->leave($__internal_4ef672fe8a7c01ad3967692f42744bc5e3016b60611eee12ac6e1df08276586d_prof);

        
        $__internal_538ed111b0f3f0e42cdbe3cad1242850832615adc154c55a85632dbd8419ebaa->leave($__internal_538ed111b0f3f0e42cdbe3cad1242850832615adc154c55a85632dbd8419ebaa_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "FOSUserBundle:Security:login.html.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}
