<?php

/* base.html.twig */
class __TwigTemplate_47fd51b039e5f4fb7e336b9f260737cefed48cbce97d781ee1d74e5c39c4c062 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8d5f11821507494ecfae54698b46d927da78604087d1953691f109161ca2dc24 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d5f11821507494ecfae54698b46d927da78604087d1953691f109161ca2dc24->enter($__internal_8d5f11821507494ecfae54698b46d927da78604087d1953691f109161ca2dc24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_a199cc7c3902c5461a29b33050b50ffda41355bbec14fd1184f1cd213af22561 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a199cc7c3902c5461a29b33050b50ffda41355bbec14fd1184f1cd213af22561->enter($__internal_a199cc7c3902c5461a29b33050b50ffda41355bbec14fd1184f1cd213af22561_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_8d5f11821507494ecfae54698b46d927da78604087d1953691f109161ca2dc24->leave($__internal_8d5f11821507494ecfae54698b46d927da78604087d1953691f109161ca2dc24_prof);

        
        $__internal_a199cc7c3902c5461a29b33050b50ffda41355bbec14fd1184f1cd213af22561->leave($__internal_a199cc7c3902c5461a29b33050b50ffda41355bbec14fd1184f1cd213af22561_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_4f36b97d87c872d645dd1596ad5797dd4f956bb62eb783629b4249079a393160 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f36b97d87c872d645dd1596ad5797dd4f956bb62eb783629b4249079a393160->enter($__internal_4f36b97d87c872d645dd1596ad5797dd4f956bb62eb783629b4249079a393160_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_1a00cd425648408c48dd1ee628c517c49428e1481567a6046679ff88da530b09 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a00cd425648408c48dd1ee628c517c49428e1481567a6046679ff88da530b09->enter($__internal_1a00cd425648408c48dd1ee628c517c49428e1481567a6046679ff88da530b09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_1a00cd425648408c48dd1ee628c517c49428e1481567a6046679ff88da530b09->leave($__internal_1a00cd425648408c48dd1ee628c517c49428e1481567a6046679ff88da530b09_prof);

        
        $__internal_4f36b97d87c872d645dd1596ad5797dd4f956bb62eb783629b4249079a393160->leave($__internal_4f36b97d87c872d645dd1596ad5797dd4f956bb62eb783629b4249079a393160_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_a3907ce268d3ccf9631a7556d383f0538b7038569430dc5a4142cd30d02c7cd7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a3907ce268d3ccf9631a7556d383f0538b7038569430dc5a4142cd30d02c7cd7->enter($__internal_a3907ce268d3ccf9631a7556d383f0538b7038569430dc5a4142cd30d02c7cd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_1c0c05a494b79bf8eae838bcd7928816a6b3495c67d0392c70af52fd69b29962 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c0c05a494b79bf8eae838bcd7928816a6b3495c67d0392c70af52fd69b29962->enter($__internal_1c0c05a494b79bf8eae838bcd7928816a6b3495c67d0392c70af52fd69b29962_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_1c0c05a494b79bf8eae838bcd7928816a6b3495c67d0392c70af52fd69b29962->leave($__internal_1c0c05a494b79bf8eae838bcd7928816a6b3495c67d0392c70af52fd69b29962_prof);

        
        $__internal_a3907ce268d3ccf9631a7556d383f0538b7038569430dc5a4142cd30d02c7cd7->leave($__internal_a3907ce268d3ccf9631a7556d383f0538b7038569430dc5a4142cd30d02c7cd7_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_191b205b497a412855260b2799fd3abf6f6f1a8884736d231cc2d7d57580bec7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_191b205b497a412855260b2799fd3abf6f6f1a8884736d231cc2d7d57580bec7->enter($__internal_191b205b497a412855260b2799fd3abf6f6f1a8884736d231cc2d7d57580bec7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_568c621b551b0fb5ec02dfdeac024c907dad310e666626224463a9a6fe1d217d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_568c621b551b0fb5ec02dfdeac024c907dad310e666626224463a9a6fe1d217d->enter($__internal_568c621b551b0fb5ec02dfdeac024c907dad310e666626224463a9a6fe1d217d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_568c621b551b0fb5ec02dfdeac024c907dad310e666626224463a9a6fe1d217d->leave($__internal_568c621b551b0fb5ec02dfdeac024c907dad310e666626224463a9a6fe1d217d_prof);

        
        $__internal_191b205b497a412855260b2799fd3abf6f6f1a8884736d231cc2d7d57580bec7->leave($__internal_191b205b497a412855260b2799fd3abf6f6f1a8884736d231cc2d7d57580bec7_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_fa2886539fbb98b65483f6f3182e7573f770b2d7468f66239a6ba9b62daac3a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa2886539fbb98b65483f6f3182e7573f770b2d7468f66239a6ba9b62daac3a4->enter($__internal_fa2886539fbb98b65483f6f3182e7573f770b2d7468f66239a6ba9b62daac3a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_38d0618985b0054e76f33f4e3c9b508ea159b3f031549ab45cdce35ce2aa5551 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38d0618985b0054e76f33f4e3c9b508ea159b3f031549ab45cdce35ce2aa5551->enter($__internal_38d0618985b0054e76f33f4e3c9b508ea159b3f031549ab45cdce35ce2aa5551_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_38d0618985b0054e76f33f4e3c9b508ea159b3f031549ab45cdce35ce2aa5551->leave($__internal_38d0618985b0054e76f33f4e3c9b508ea159b3f031549ab45cdce35ce2aa5551_prof);

        
        $__internal_fa2886539fbb98b65483f6f3182e7573f770b2d7468f66239a6ba9b62daac3a4->leave($__internal_fa2886539fbb98b65483f6f3182e7573f770b2d7468f66239a6ba9b62daac3a4_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/var/www/html/ex60/hw60/app/Resources/views/base.html.twig");
    }
}
