<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_8f797635c6560fcf0931d89d6d05cc91c8d4cbbe7d51be4498a2d1830255611d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fd8c89860d12588de56699aa1923ecf0986147aff33109b93a9c5ac2a52e781d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd8c89860d12588de56699aa1923ecf0986147aff33109b93a9c5ac2a52e781d->enter($__internal_fd8c89860d12588de56699aa1923ecf0986147aff33109b93a9c5ac2a52e781d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $__internal_b8a88fcd0de74bdc0272a6fe25245e62e00445521fe8b170e81cb0d9fbbd7666 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a88fcd0de74bdc0272a6fe25245e62e00445521fe8b170e81cb0d9fbbd7666->enter($__internal_b8a88fcd0de74bdc0272a6fe25245e62e00445521fe8b170e81cb0d9fbbd7666_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fd8c89860d12588de56699aa1923ecf0986147aff33109b93a9c5ac2a52e781d->leave($__internal_fd8c89860d12588de56699aa1923ecf0986147aff33109b93a9c5ac2a52e781d_prof);

        
        $__internal_b8a88fcd0de74bdc0272a6fe25245e62e00445521fe8b170e81cb0d9fbbd7666->leave($__internal_b8a88fcd0de74bdc0272a6fe25245e62e00445521fe8b170e81cb0d9fbbd7666_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a6b3505b8ad6e366f29b9cf30ef60f76f5a7ce4328eb04ff21c46ca3ae5ab4f2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6b3505b8ad6e366f29b9cf30ef60f76f5a7ce4328eb04ff21c46ca3ae5ab4f2->enter($__internal_a6b3505b8ad6e366f29b9cf30ef60f76f5a7ce4328eb04ff21c46ca3ae5ab4f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_422ed724490e9a68e5e1d05d5c4960ea19943fb8b445f36cd92916dd77823489 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_422ed724490e9a68e5e1d05d5c4960ea19943fb8b445f36cd92916dd77823489->enter($__internal_422ed724490e9a68e5e1d05d5c4960ea19943fb8b445f36cd92916dd77823489_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_422ed724490e9a68e5e1d05d5c4960ea19943fb8b445f36cd92916dd77823489->leave($__internal_422ed724490e9a68e5e1d05d5c4960ea19943fb8b445f36cd92916dd77823489_prof);

        
        $__internal_a6b3505b8ad6e366f29b9cf30ef60f76f5a7ce4328eb04ff21c46ca3ae5ab4f2->leave($__internal_a6b3505b8ad6e366f29b9cf30ef60f76f5a7ce4328eb04ff21c46ca3ae5ab4f2_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:register.html.twig", "/var/www/html/ex60/hw60/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
