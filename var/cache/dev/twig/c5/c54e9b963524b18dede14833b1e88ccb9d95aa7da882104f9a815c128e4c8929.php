<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_37e46c76d02729b1ef2fea2576c1b1aba23cda9eef4c1c20a78c979265363716 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_79cb28c94491c4bd320fb2e4ea271a039ec59d3b5d93306760b3a847f006ed1d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79cb28c94491c4bd320fb2e4ea271a039ec59d3b5d93306760b3a847f006ed1d->enter($__internal_79cb28c94491c4bd320fb2e4ea271a039ec59d3b5d93306760b3a847f006ed1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_4d9693842ac977eebabe17b2520c43c8c0d25ccaee8e02c9d0f059f779301186 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d9693842ac977eebabe17b2520c43c8c0d25ccaee8e02c9d0f059f779301186->enter($__internal_4d9693842ac977eebabe17b2520c43c8c0d25ccaee8e02c9d0f059f779301186_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_79cb28c94491c4bd320fb2e4ea271a039ec59d3b5d93306760b3a847f006ed1d->leave($__internal_79cb28c94491c4bd320fb2e4ea271a039ec59d3b5d93306760b3a847f006ed1d_prof);

        
        $__internal_4d9693842ac977eebabe17b2520c43c8c0d25ccaee8e02c9d0f059f779301186->leave($__internal_4d9693842ac977eebabe17b2520c43c8c0d25ccaee8e02c9d0f059f779301186_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
