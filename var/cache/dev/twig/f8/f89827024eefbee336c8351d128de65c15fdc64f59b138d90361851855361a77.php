<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_35ac850a0417c3a4a8a05e667660d9f613d834031aa28d5e26ef72805f4706a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_39a00c687f5dd2d8c16e827546affe38d1108459296253e18d118b387209c65d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39a00c687f5dd2d8c16e827546affe38d1108459296253e18d118b387209c65d->enter($__internal_39a00c687f5dd2d8c16e827546affe38d1108459296253e18d118b387209c65d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        $__internal_44363a1c1307216f0f2d20323ba287dc3d10048c9287c457d86faf683f0601b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_44363a1c1307216f0f2d20323ba287dc3d10048c9287c457d86faf683f0601b1->enter($__internal_44363a1c1307216f0f2d20323ba287dc3d10048c9287c457d86faf683f0601b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")), "exception" => $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_39a00c687f5dd2d8c16e827546affe38d1108459296253e18d118b387209c65d->leave($__internal_39a00c687f5dd2d8c16e827546affe38d1108459296253e18d118b387209c65d_prof);

        
        $__internal_44363a1c1307216f0f2d20323ba287dc3d10048c9287c457d86faf683f0601b1->leave($__internal_44363a1c1307216f0f2d20323ba287dc3d10048c9287c457d86faf683f0601b1_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "TwigBundle:Exception:exception.json.twig", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}
