<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_18776b82c176eb20be163d00ab4ba020495e380c5f58b67ee2829f732ab7e253 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bdbbbd355e0e4aa19ee8845a24d8518733273fe36878f7fc2474bebcda832df2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bdbbbd355e0e4aa19ee8845a24d8518733273fe36878f7fc2474bebcda832df2->enter($__internal_bdbbbd355e0e4aa19ee8845a24d8518733273fe36878f7fc2474bebcda832df2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_96a8fbf181af9fa8a408ffe68a0d1ee15d54656e812b7b28c7fbac5a454c2d5d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_96a8fbf181af9fa8a408ffe68a0d1ee15d54656e812b7b28c7fbac5a454c2d5d->enter($__internal_96a8fbf181af9fa8a408ffe68a0d1ee15d54656e812b7b28c7fbac5a454c2d5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_bdbbbd355e0e4aa19ee8845a24d8518733273fe36878f7fc2474bebcda832df2->leave($__internal_bdbbbd355e0e4aa19ee8845a24d8518733273fe36878f7fc2474bebcda832df2_prof);

        
        $__internal_96a8fbf181af9fa8a408ffe68a0d1ee15d54656e812b7b28c7fbac5a454c2d5d->leave($__internal_96a8fbf181af9fa8a408ffe68a0d1ee15d54656e812b7b28c7fbac5a454c2d5d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
