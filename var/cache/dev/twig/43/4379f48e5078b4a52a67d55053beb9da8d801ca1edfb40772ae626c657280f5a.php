<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_119c72e7dff84766de0fd2a32db5fd6c286207212ae9347f60b3d1e16520fd97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3ee7f036fc499bd397e4c0f4ea003d2d302623917410b52c8b0c607d8300d3b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3ee7f036fc499bd397e4c0f4ea003d2d302623917410b52c8b0c607d8300d3b8->enter($__internal_3ee7f036fc499bd397e4c0f4ea003d2d302623917410b52c8b0c607d8300d3b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_0fd3e42fba732cf3344f5108bdda1f063ae492a2051601ffbe8e7da93f8fac6f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0fd3e42fba732cf3344f5108bdda1f063ae492a2051601ffbe8e7da93f8fac6f->enter($__internal_0fd3e42fba732cf3344f5108bdda1f063ae492a2051601ffbe8e7da93f8fac6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_3ee7f036fc499bd397e4c0f4ea003d2d302623917410b52c8b0c607d8300d3b8->leave($__internal_3ee7f036fc499bd397e4c0f4ea003d2d302623917410b52c8b0c607d8300d3b8_prof);

        
        $__internal_0fd3e42fba732cf3344f5108bdda1f063ae492a2051601ffbe8e7da93f8fac6f->leave($__internal_0fd3e42fba732cf3344f5108bdda1f063ae492a2051601ffbe8e7da93f8fac6f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
