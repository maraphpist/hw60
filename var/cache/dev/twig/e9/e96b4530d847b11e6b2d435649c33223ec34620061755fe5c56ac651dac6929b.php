<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_bf1e9f33d8870d9f7194eb22f9bd0f79ed8c8337b2955d3761d55338cfa86369 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1c49bbdfcad5a9337ef42f3119f55addb89e959f6521dacea4d3811613e8553e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c49bbdfcad5a9337ef42f3119f55addb89e959f6521dacea4d3811613e8553e->enter($__internal_1c49bbdfcad5a9337ef42f3119f55addb89e959f6521dacea4d3811613e8553e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_39e687a8cca8e629d5a950402eb695fe56a5341baa2a0b752012d5cc02d8328c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_39e687a8cca8e629d5a950402eb695fe56a5341baa2a0b752012d5cc02d8328c->enter($__internal_39e687a8cca8e629d5a950402eb695fe56a5341baa2a0b752012d5cc02d8328c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_1c49bbdfcad5a9337ef42f3119f55addb89e959f6521dacea4d3811613e8553e->leave($__internal_1c49bbdfcad5a9337ef42f3119f55addb89e959f6521dacea4d3811613e8553e_prof);

        
        $__internal_39e687a8cca8e629d5a950402eb695fe56a5341baa2a0b752012d5cc02d8328c->leave($__internal_39e687a8cca8e629d5a950402eb695fe56a5341baa2a0b752012d5cc02d8328c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}
