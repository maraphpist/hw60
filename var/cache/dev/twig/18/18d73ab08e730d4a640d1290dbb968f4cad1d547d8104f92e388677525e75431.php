<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_d1f22b4d567290d22c377e28534b534b6be4ca8229b3cd7918f74ffa5c91100c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b5991ae62c388a69dc384f3b65aa610b469d91f5a9803d64fc9cd9634b8f8713 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b5991ae62c388a69dc384f3b65aa610b469d91f5a9803d64fc9cd9634b8f8713->enter($__internal_b5991ae62c388a69dc384f3b65aa610b469d91f5a9803d64fc9cd9634b8f8713_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        $__internal_b3c7fd1e39541d70ac19d8590a0f62eb536a6aae6c5966fbb58e7fc51c27a145 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b3c7fd1e39541d70ac19d8590a0f62eb536a6aae6c5966fbb58e7fc51c27a145->enter($__internal_b3c7fd1e39541d70ac19d8590a0f62eb536a6aae6c5966fbb58e7fc51c27a145_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_b5991ae62c388a69dc384f3b65aa610b469d91f5a9803d64fc9cd9634b8f8713->leave($__internal_b5991ae62c388a69dc384f3b65aa610b469d91f5a9803d64fc9cd9634b8f8713_prof);

        
        $__internal_b3c7fd1e39541d70ac19d8590a0f62eb536a6aae6c5966fbb58e7fc51c27a145->leave($__internal_b3c7fd1e39541d70ac19d8590a0f62eb536a6aae6c5966fbb58e7fc51c27a145_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/checkbox_widget.html.php", "/var/www/html/ex60/hw60/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/checkbox_widget.html.php");
    }
}
